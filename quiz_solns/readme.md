## Quiz solutions

As I've pointed out during lecture, the purpose of having quizzes is to point
out specific tools and/or topics that you should be aware of. The following list
details not only said tool/topic, but also provides hints or partial solutions
to previous quizzes.

*   Quiz 1: The function that I asked you to integrate, namely $f(z) =
    \mathrm{e}^{z}$, has [infinitely many] anti-derivatives. The value of the
    integral then can be computed by selecting a specific antiderivative, say
    $F(z) = \mathrm{e}^{z}$, and the subtracting the values of said function at
    the endpoints of the contour of integration.

    In symbols:

    $$\int_{-i}^{i} \mathrm{e}^{z}\,\mathrm{d}z =
    \mathrm{e}^{i}-\mathrm{e}^{-i}$$

*   Quiz 2: The function that I asked you to integrate, namely
    $\frac{1}{z^{2}+1}$, is differentiable everywhere except at the points $z =
    i$, and $z = -i$; however, both of these points are located _outside of the
    contour of integration_. Hence, by _Cauchy's Integral Theorem_, the integral
    is equal to 0.

*   Quiz 3: _Does $f$ entire and bounded implies $f$ constant?_ **Yes, it
    does.** I proved this during lecture. Also, see problem 1 in midterm 2.

---

[Return to main course website][MAIN]

[MAIN]: ..

