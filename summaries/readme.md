# Lecture summaries

Over the weekends I will review the material discussed over the week and will
post here the main topics/ideas that were covered. In a sense, the information
listed below can be regarded as a sort of _study guide_ that should complement
the ["[p]ractice material"][pm] section of our class website.

---

## Week 8

*   _Friday lecture_
    -   Taylor Series: _proof of theorem 3 (page 243)_

*   _Wednesday lecture_
    -   _More consequences of Cauchy's integral formula_: bounds for analytic
        functions.
    -   The fundamental theorem of algebra.
    -   The maximum modulus principle.

*   _Monday lecture_
    -   Midterm 2

## Week 7

*   _Friday lecture_
    -   Complex integration: _consequences of Cauchy's integral formula_

*   _Wednesday lecture_
    -   Complex integration: _Cauchy's integral formula_

*   _Monday lecture_
    -   No lecture (_veterans day_).

## Week 6

> _Note:_
>
> I kind of lost track of the topics covered on specific days, so I'm listing
> them all at once.

*   _Monday through Friday_

    -   Theorem 7 (page 176)
    -   A _crash course_ on 2-D integral calculus:
        +   integrals of scalar functions and vector fields along a _curve_ in
            $\mathbb{R}^{2}$.
        +   review of _Green's theorem_
    -   Cauchy's Integral theorem: _integrals over closed loops of anlytic
        functions are equal to 0_.
    -   Cauchy's Integral theorem: _examples and applications_.

## Week 5

*   _Friday lecture_
    -   Complex integration: _the beginnings of path independence_

*   _Wednesday lecture_
    -   Complex integration: _integration via paramatrization_
    -   Complex integration: _integration via an anti-derivative_
    -   Complex integration: _integrating from $-i$ to $i$ in several different
        ways._  

*   _Monday lecture_
    -   No lecture (_Getty fire_).

## Week 4

*   _Friday lecture_
    -   Complex integration: _smooth arcs_, _closed curves_, and contours.
    -   Complex integration: parametrization of line segments, rays, and
        circumference arcs.

*   _Wednesday lecture_
    -   The complex exponential $z^{\alpha}$ (the case when $\alpha$ is
        rational).
    -   Introduction to complex integration: _Riemann sums_

*   _Monday lecture_
    -   Midterm 1

## Week 3

*   _Friday lecture_
    -   The complex exponential $z^{\alpha}$.

*   _Wednesday lecture_
    -   The complex multi-valued _"function"_ $\log z$.
    -   The complex single-valued function $\mathrm{Log}\,z$ (principal branch).

*   _Monday lecture_
    -   Applications of _Cauchy-Riemann_: Harmonic functions
    -   Applications of _Cauchy-Riemann_: The harmonic conjugate
        +    Equipotentials meet at right angles.
    -   Applications of _Cauchy-Riemann_: Steady state solutions of the _heat
        equation_*

## Week 2

*   _Friday lecture_
    -   Applications of _Cauchy-Riemann_: The complex exponential.
        +   Trying to make sense of the complex logarithm.
*   _Wednesday lecture_
    -   Examples of limits and continuity.
    -   The definition of the complex derivative.
    -   The _Cauchy-Riemann_ equations.
*   _Monday lecture_
    -   Stereographic projection (continued).
    -   Brief (and rather incomplete) definition of _complex valued_ functions.
    -   Crash review course on limits and continuity.


## Week 1

*   _Friday lecture_
    -   Powers of complex numbers (the easy case, $z^{n}$, $n=0,1,\dots$).
        +   _De Moivre's_ formula.
    -   Roots of complex numbers (the easy case, see above).
    -   Stereographic projection.
*   _Wednesday lecture_
    -   Polar coordinates (the purely imaginary complex exponential)
    -   Other definitions: _modulus_, _argument_, _complex conjugate_, etc.
    -   The triangle inequality.
*   _Monday lecture_
    -   Extending sets to solve polynomial equations (continued).
    -   Polar coordinates.


## Week 0

*   _Friday lecture_
    -   Syllabus overview.
    -   Extending sets to solve polynomial equations.

---


[Return to main course website][MAIN]


[pm]: ../practice/
[MAIN]: ..

