# MATH 132: Complex Analysis for Applications (Fall 2019)

This is to be considered the class website. Here you will find material related
to the concepts we discuss during lecture.

## Sections

*   [The class syllabus][syllabus] ([`.pdf` version][syllabus-pdf])
*   [Weekly summaries][summary]
*   [Practice material: old exams and/or exercises][practice]
*   [Homework assignments][hw]
*   [Course handouts][handouts]
*   [Quiz solutions][qs]


[syllabus]: syllabus/
[syllabus-pdf]: syllabus/readme.pdf
[hw]: assignments/
[summary]: summaries/
[practice]: practice/
[handouts]: handouts/
[qs]: quiz_solns/


## Additional resources

Here are other sites associated to our course:

*   [The CCLE class website][CCLE]: this will be used mostly for class
    announcements.

*   [Discussion section material][ds]: Your TA might decide to post discussion
    material for the benefit of the class as a whole.


[CCLE]: https://ccle.ucla.edu/course/view/19F-MATH132-2
[ds]: https://www.bertrandstone.com/ta/
