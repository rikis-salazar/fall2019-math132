# _A solution to problem 20, section 1.4_

> **Note:**
>
> This problem turns out to require a couple of _tricks_ that some of you might
> not have encountered before. As such, it might end up being quite frustrating,
> and might make you feel less confident about your understanding of polar
> coordinates. These types of problems (_e.g.,_ those requiring tricks) are not
> suitable for exams, as they do not provide a clear idea of whether or not you
> are understanding the material presented during lecture.

This problem asks you to prove the rather well know identity
$$1 + z + z^{2} + \cdots + z^{n} = \frac{1 - z^{n+1}}{1 - z},\qquad z\neq 1.$$

One way to proceed, is to let $S$ be the left hand side of the equation above,
and realize that $zS$ almost matches the _contents_ of $S$. In symbols:
\begin{align*}
  S & = 1 + z + z^{2} + \cdots + z^{n} \\
  zS & = z + z^{2} + \cdots + z^{n} + z^{(n+1)}.
\end{align*}
The difference $S - zS$ contains only two terms. That is
$$S - zS = 1 - z^{n+1}.$$

On the other hand, the difference $S - zS$ can also be factored out as $S(1-z)$.
This leads to the identity
$$S(1-z) = 1 - z^{n+1},$$
which holds for all values of $z$. If we exclude the value $z=1$, then we can
divide by the term $1-z$ to obtain the desired formula
$$1 + z + z^{2} + \cdots + z^{n} = \frac{1 - z^{n+1}}{1-z},\qquad z\neq 1.$$

Next, notice that we can apply the formula above to complex numbers with modulus
equal to one (except of course, $z=1$). That is we can let $z =
\mathrm{e}^{i\theta}$, where $0<\theta<2\pi$.

The equation above then becomes
$$1 + \mathrm{e}^{i\theta} + \mathrm{e}^{i2\theta} + \mathrm{e}^{i3\theta} +
  \cdots + \mathrm{e}^{in\theta} = \frac{1 - \mathrm{e}^{i(n+1)\theta}}
  {1 - \mathrm{e}^{i\theta}},$$
and by breaking this number down into its real and imaginary parts we obtain
\begin{align*}
  1 + \cos\theta + \cos 2\theta + \cos 3\theta + \cdots + \cos n\theta &
  = \mathbf{Re}(\mathrm{RHS}), \\
  \sin\theta + \sin 2\theta + \sin 3\theta + \cdots + \sin n\theta &
  = \mathbf{Im}(\mathrm{RHS}),
\end{align*}
where
$$\mathrm{RHS} =
  \frac{1 - \mathrm{e}^{i(n+1)\theta}}{1 - \mathrm{e}^{i\theta}}.$$


## The first trick: a not-so-intuitive factorization

Note that all terms in RHS, namely, $1$, $\mathrm{e}^{i\theta}$, and
$\mathrm{e}^{i(n+1)\theta}$, can be expressed in terms of
$\mathrm{e}^{i\theta/2}$.

$$1 = \mathrm{e}^{i\theta/2} \mathrm{e}^{-i\theta/2},\qquad
  \mathrm{e}^{i\theta} = \mathrm{e}^{i\theta/2} \mathrm{e}^{i\theta/2},\qquad
  \mathrm{e}^{i(n+1)\theta} = \mathrm{e}^{i\theta/2}
  \mathrm{e}^{i(n+1/2)\theta}.$$

Thus, we have
$$\mathrm{RHS} =
  \frac{ \mathrm{e}^{i\theta/2} (\mathrm{e}^{-i\theta/2} - \mathrm{e}^{i(n+1/2)\theta}) }
  { \mathrm{e}^{i\theta/2} (\mathrm{e}^{-i\theta/2} - \mathrm{e}^{i\theta/2}) }
  =
  \frac{ \mathrm{e}^{-i\theta/2} - \mathrm{e}^{i(n+1/2)\theta} }
  { \mathrm{e}^{-i\theta/2} - \mathrm{e}^{i\theta/2} }.$$

Next up, notice that
\begin{align*}
  \mathrm{e}^{-i\theta/2} - \mathrm{e}^{i\theta/2}
  & = \cos(-\theta/2) + i\sin(-\theta/2)
    - \big( \cos(\theta/2) + i\sin(\theta/2) \big) \\
  & = \cos(\theta/2) - i\sin(\theta/2)
    - \cos(\theta/2) - i\sin(\theta/2) \\
  & = -2i \sin(\theta/2).
\end{align*}

Also, since $(-i)i = 1$, we have $1/(-i) = i$. Or in other words, a negative $i$
in the denominator can be brought up as a positive $i$ in the numerator (and
_vice versa_). Our RHS now looks like this
$$\mathrm{RHS} =
  \frac{ i(\mathrm{e}^{-i\theta/2} - \mathrm{e}^{i(n+1/2)\theta}) }
  {2\sin(\theta/2)}.$$

The expression in the numerator is then
\begin{align*}
  i(\mathrm{e}^{-i\theta/2} - \mathrm{e}^{i(n+1/2)\theta})
  & = i\mathrm{e}^{-i\theta/2} -i\mathrm{e}^{i(n+1/2)\theta} \\
  & = i\big( \cos(\theta/2) - i\sin(\theta/2) \big)
    - i\big( \cos((n+1/2)\theta) + i\sin((n+1/2)\theta) \big) \\
  & = \sin(\theta/2) + \sin((n+1/2)\theta)
    + i\big( \cos(\theta/2) - \cos((n+1/2)\theta) \big).
\end{align*}

With both expressions (numerator and denominator) at hand we can rewrite RHS. We have
$$RHS = \frac{ \sin(\theta/2) + \sin((n+1/2)\theta) }{2\sin(\theta/2)}
  + i \frac{ \cos(\theta/2) - \cos((n+1/2)\theta) }{2\sin(\theta/2)}.$$

On the other hand, recalling that
$$RHS = 1 + \cos\theta + \cos 2\theta + \cdots + \cos n\theta
  + i ( \sin\theta + \sin 2\theta + \cdots + \sin n\theta ),$$
we can then conclude that
\begin{align*}
  1 + \cos\theta + \cos 2\theta + \cdots + \cos n\theta 
  & = \frac{1}{2} + \frac{\sin((n+1/2)\theta)}{2\sin(\theta/2)}, \\
  \sin\theta + \sin 2\theta + \cdots + \sin n\theta
  & = \frac{ \cos(\theta/2) - \cos((n+1/2)\theta) }{2\sin(\theta/2)}.
\end{align*}


## The second trick: a not-so-intuitive set of identities

Here the idea is to _somehow_ express the arguments in the cosine functions,
namely $\theta/2$, and $(n+1/2)\theta$, in terms of the angle $n\theta/2$. Here
is the way to do it:
\begin{align*}
  \theta/2 & = \theta/2 + n\theta/2 - n\theta/2
  = (n\theta/2 + \theta/2) - n\theta/2 = (n+1)\theta/2 - n\theta/2
  = \alpha - \beta, \\
  (n+1/2)\theta & = n\theta/2 + n\theta/2 + \theta/2
  = (n\theta/2 + \theta/2) + n\theta/2 = (n+1)\theta/2 + n\theta/2
  = \alpha + \beta,
\end{align*}
where $\alpha = (n+1)\theta/2$, and $\beta = n\theta/2$.

With this setup in place we have
\begin{align*}
  \cos(\theta/2) & = \cos(\alpha-\beta) = \cos(\alpha)\cos(\beta) + \sin(\alpha)\sin(\beta), \\
  \cos((n+1/2)\theta) & = \cos(\alpha+\beta) = \cos(\alpha)\cos(\beta) - \sin(\alpha)\sin(\beta),
\end{align*}
which leads to
$$\cos(\theta/2) - \cos((n+1/2)\theta) = 2\sin(\alpha)\sin(\beta)
  =  2\sin((n+1)\theta/2)\sin(n\theta/2).$$

The second identity can now be established
\begin{align*}
  \sin\theta + \sin 2\theta + \cdots + \sin n\theta
  & = \frac{\cos(\theta/2) - \cos((n+1/2)\theta)}{2\sin(\theta/2)} \\
  & = \frac{2\sin((n+1)\theta/2)\sin(n\theta/2)}{2\sin(\theta/2)}.
\end{align*}

That is
$$\sin\theta + \sin 2\theta + \cdots + \sin n\theta
  = \frac{\sin((n+1)\theta/2)\sin(n\theta/2)}{\sin(\theta/2)}.$$

---

[Click here to access a `.pdf` version of this handout][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

