# _About that "punctured disk" comment in Midterm 1_

> **Note:**
>
> In one of the problems of this past midterm 1 most of you received a small
> penalty (-1 point) as well as comment pointing out that you missed to consider
> the case where a certain function vanishes inside a _punctured disk_. It turns
> out that the hypothesis of the problem prevent this situation to occur. In
> this document I explain why this is the case.

## The statement of the problem

Prove _L’Hôpital’s_ rule: if the complex-valued functions $f(z)$ and
$g(z)$ are analytic at $z_{0}$ and $f(z_{0}) = g(z_{0}) = 0$, but $g'(z_{0})
\neq 0$, then
$$\lim_{z\to z_{0}} \frac{f(z)}{g(z)} = \frac{f'(z_{0})}{g'(z_{0})}.$$


## The _incomplete_ proof

Most students wrote something along the lines of

> Since $f(z_{0}) = g(z_{0}) = 0$, we have 
> \begin{align*}
>   \frac{f(z)}{g(z)} & = \frac{f(z)-f(z_{0})}{g(z)-g(z_{0})}.
> \end{align*}
> Multiplying and dividing by $z-z_{0}$ then leads to
> \begin{align*}
>   \frac{f(z)}{g(z)} & = \frac{f(z)-f(z_{0})}{z-z_{0}}\cdot\frac{z-z_{0}}{g(z)-g(z_{0})}
>   = \frac{\frac{f(z)-f(z_{0})}{z-z_{0}}}{\frac{g(z)-g(z_{0})}{z-z_{0}}},
> \end{align*}
> and since the limits in the numerator and the denominator of the latter
> expression exist (and equal $f'(z_{0})$, and $g'(z_{0})$, respectively), and
> the limit in the denominator is not zero, we have
> \begin{align*}
>   \lim_{z\to z_{0}}\frac{f(z)}{g(z)}
>   = \frac{\lim_{z\to z_{0}} \frac{f(z)-f(z_{0})}{z-z_{0}}}
>          {\lim_{z\to z_{0}} \frac{g(z)-g(z_{0})}{z-z_{0}}}
>   = \frac{f'(z_{0})}{g'(z_{0})}.
> \end{align*}


## The _problem_ with the proof above

The argument above works well in cases where the quotient $f(z)/g(z)$ is well
defined. In other words, by taking the limit as $z$ tends to $z_{0}$ without
ruling the possibility of $g$ vanishing near $z_{0}$, one is implicitly assuming
that $g(z)\neq 0$ in a region near $z_{0}$.


## The _missing_ argument

If the _implicit assumption_ were to be false, that is, if no matter how close
we are to $z_{0}$ we couldn't guarantee that $g$ is non-vanishing, this means
that for every positive $\delta$, the disk
$$D_{\delta} = \{ z \in \mathbb{C}\,:\, |z - z_{0}| < \delta \}$$
contains a point different than $z_{0}$ such that $g$ vanishes at this point.

This will eventually prove to be impossible given the assumptions of the
problem. More specifically, the assumption that $g'(z_{0}) \neq 0$. To obtain a
contradiction, we can choose specific values of $\delta$ to produce a sequence
of points. We can then use this sequence to approach $g'(z_{0})$. Here are the
details:

Let $\delta_{n} = 2^{-n}$, $n = 1,2,\dots$ and let $z_{n}$ be a point in the
punctured disk
$$D_{\delta_{n}}\mathbin{\backslash}\{z_{0}\} = \{ z \in \mathbb{C}\,:\, 0 < |z - z_{0}| < \delta \}$$
such that $g$ vanishes at $z_{n}$.

By construction, the points in the sequence $\{ z_{n} \}_{n=1}^{\infty}$ satisfy
$$0 < |z_{n}-z_{0}| < \frac{1}{2^{n}}$$
and
$$g(z_{n}) = 0$$
for all values of $n$.

If we use this sequence to compute the derivative of $g$, we have on the one
hand
$$g'(z_{0}) = \lim_{n\to\infty} \frac{g(z_{n})-g(z_{0})}{z_{n}-z_{0}}$$
because $z_{n}$ tends to $z_{0}$.

On the other hand
$$\lim_{n\to\infty} \frac{g(z_{n})-g(z_{0})}{z_{n}-z_{0}} 
  = \lim_{n\to\infty} \frac{0 - 0}{z_{n}-z_{0}}
  = \lim_{n\to\infty} 0 = 0.$$

> _Note:_ in this case the denominator of the quotient is not zero because the
> points $z_{n}$ satisfy the inequality $0<|z_{n}-z_{0}|$.

The two observations above lead to $g'(z_{0}) = 0$ which directly contradicts
one of the assumptions of the original problem. This means that we do not have
to worry about the function $g$ vanishing near $z_{0}$ as this contradiction
shows that there exists at least one value $\tilde{\delta} > 0$, such that $g$
is non-vanishing in the punctured disk
$D_{\tilde{\delta}}\mathbin{\backslash}\{z_{0}\}$;
within this punctured disk we can safely _deal with_ the quotient $f(z)/g(z)$,
and the proof presented above is enough to establish this weak version of
_L’Hôpital’s_ rule.

---

[Click here to access a `.pdf` version of this handout][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

