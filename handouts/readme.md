# Miscellaneous handouts

In this section you will find material that is related to the contents we are
discussing during lecture, however, presenting such material during class will
either take extra time (hence reducing the amount of time spent dedicated to
other topics), or will not add anything new to our general discussion.

It is my hope that at some point you go over the contents of these handouts
without feeling pressured to study them as they are not a good fit for a midterm
nor a final exam.

1.  [_A solution to problem 20, section 1.4_][h1]
1.  [_Cauchy Riemann equations + continuity = differentiability_][h2]
1.  [_About that "punctured disk" comment in Midterm 1_][h3]
1.  [_Cauchy's integral formula for the derivative of a function_][h4]

---

[Return to main course website][MAIN]


[h4]: handout4/
[h3]: handout3/
[h2]: handout2/
[h1]: handout1/

[MAIN]: ..

