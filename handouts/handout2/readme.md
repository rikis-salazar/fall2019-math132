# _Cauchy-Riemann + continuity = differentiability_

> **Note:**
>
> For the time being, this document will only lay out the basic ideas, as well
> as the important steps needed to establish the result. In the ear future I
> will turn it into a more complete handout.

_Basic idea:_ to note that if a function $f$ is differentiable (say at $x_{0}$),
then near this point we have
$$f(x) - f(x_{0}) \approx f'(x_{0})(x-x_{0}).$$

_The basic problem:_ we want to deal with an actual equality (as opposed to an
approximation).

_The solution in 1-D:_ use a combination of the _mean value theorem_ as well as
continuity of $f'$ to argue you that
$$f(x) - f(x_{0}) = f'(\theta_{x,x_{0}})(x-x_{0})
  = \big(f'(x_{0}) + \epsilon(x) \big)(x-x_{0}),$$ 
where $\theta_{x,x_{0}}$ is a point _between_ $x$ and $x_{0}$; and $\epsilon(x)$
tends to 0 as $x$ tends to $x_{0}$.

> _Note:_ The _error_ in the approximation is 
> $$\epsilon(x) = f'(\theta_{x,x_{0}}) - f'(x_{0}).$$
> Since $\theta_{x}$ _lives between_ $x$ and $x_{0}$, as $x$
> tends to $x_{0}$, $\theta_{x,x_{0}}$ is _squeezed_ down to $x_{0}$; the continuity
> of $f'$ now guarantees that $\epsilon(x)$ tends to 0 as $x$ approaches
> $x_{0}$ .

_The actual problem:_ $f$ is complex valued (_i.e.,_ has a real and an imaginary
part, both of which depend on two variables).

_The actual solution_: break $f$ down to its real and imaginary parts, and
generalize the continuity argument.

Let us write $f(z) = u(x,y) + iv(x,y)$, $z=x+iy$, and let us fix a point $z_{0}
= x_{0} + iy_{0}$. Then
\begin{align*}
  f(z) - f(z_{0})
  & = u(x,y) +iv(x,y) - \big(u(x_{0},y_{0})+iv(x_{0},y_{0})\big) \\
  & = u(x,y) - u(x_{0},y_{0}) + i\big(v(x,y)-v(x,y)\big)
\end{align*}

We now focus on the differences $u(x,y)-u(x_{0},y_{0})$ as well as
$v(x,y)-v(x_{0},y_{0})$. The strategy is to introduce a _mixed term_ (_e.g.,_
$u(x_{0},y)$) to make sure the functions are only changing in one direction at a
time. For the difference $u(x,y) - u(x_{0},y_{0})$, we have
\begin{align*}
  u(x,y) - u(x_{0},y_{0})
  & = \underbrace{u(x,y) - u(x_{0},y)}_{\mathrm{(I)}}
    + \underbrace{u(x_{0},y) - u(x_{0},y_{0})}_{\mathrm{(II)}},
\end{align*}
the mean value therem now gives
\begin{align*}
  u(x,y) - u(x_{0},y)
  & = \frac{\partial u}{\partial x}\big(\theta_{x,x_{0}},y\big)(x-x_{0}), & & \mathrm{(I)} \\
  u(x_{0},y) - u(x_{0},y_{0})
  & = \frac{\partial u}{\partial y}\big(x_{0},\eta_{y,y_{0}}\big)(y-y_{0}), & & \mathrm{(II)}
\end{align*}
which in turn leads to
\begin{align*}
  u(x,y) - u(x_{0},y_{0})
  & = \Big[ \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big) + \epsilon_{1}(x,y) \Big](x-x_{0}),
  & & \mathrm{(I)}\\
  u(x_{0},y) - u(x_{0},y_{0})
  & = \Big[ \frac{\partial u}{\partial y}\big(x_{0},y_{0}\big) + \epsilon_{2}(x,y) \Big](y-y_{0}),
  & & \mathrm{(II)}
\end{align*}
where
\begin{align*}
  \epsilon_{1}(x,y) 
  & = \frac{\partial u}{\partial x}\big(\theta_{x,x_{0}},y\big)
    - \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big), \\
  \epsilon_{2}(x,y)
  & = \frac{\partial u}{\partial y}\big(x_{0},\eta_{y,y_{0}}\big)
    - \frac{\partial u}{\partial y}\big(x_{0},y_{0}\big),
\end{align*}
are such that both functions tend to 0 as $z-z_{0} = (x-x_{0})+i(y-y_{0})$
tends to 0, provided the partial derivatives of $u(x,y)$ are continuous at
$(x_{0},y_{0})$.

Summarizing, by $\mathrm{(I)}$ and $\mathrm{(II)}$, we have
\begin{align*}
  u(x,y) - u(x_{0},y_{0})
  & = \Big[ \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big) + \epsilon_{1}(x,y) \Big](x-x_{0})
    + \Big[ \frac{\partial u}{\partial y}\big(x_{0},y_{0}\big) + \epsilon_{2}(x,y) \Big] (y-y_{0}).
\end{align*}

The _Cauchy-Riemann_ equations lead to
\begin{align*}
  u(x,y) - u(x_{0},y_{0})
  & = \Big[ \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big) + \epsilon_{1}(x,y) \Big](x-x_{0})
    + \Big[ -\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) + \epsilon_{2}(x,y) \Big] (y-y_{0}) 
  & & \\
  & = \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)(x-x_{0})
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) \big(i(y-y_{0})\big)
    + \epsilon_{5}(x,y),
  & & \mathrm{(III)}
\end{align*}
where $\epsilon_{5}(x,y) = \epsilon_{1}(x,y)(x-x_{0}) + \epsilon_{2}(x,y)(y-y_{0})$.

A similar argument applied to the difference $v(x,y)-v(x_{0},y_{0})$ leads to
\begin{align*}
  i \big( v(x,y) - v(x_{0},y_{0}) \big)
  & = \Big[ i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) + i\epsilon_{3}(x,y) \Big](x-x_{0})
    + \Big[ i\frac{\partial v}{\partial y}\big(x_{0},y_{0}\big) + i\epsilon_{4}(x,y) \Big] (y-y_{0})
  & & \\
  & = \Big[ i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) + i\epsilon_{3}(x,y) \Big](x-x_{0})
    + \Big[ i\frac{\partial u}{\partial x}\big(x_{0},y_{0}\big) + i\epsilon_{4}(x,y) \Big] (y-y_{0})
  & & \\
  & = i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big)(x-x_{0})
    + \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)\big(i(y-y_{0})\big)
    + \epsilon_{6}(x,y),
  & & \mathrm{(IV)}
\end{align*}
where $\epsilon_{6}(x,y) = i\epsilon_{3}(x,y)(x-x_{0}) + i\epsilon_{4}(x,y)(y-y_{0})$.

Formulas $\mathrm{(III)}$ and $\mathrm{(IV)}$ are the two terms that appear in
the original difference $f(z)-f(z_{0})$. They lead to
\begin{align*}
  f(z)-f(z_{0})
  & = \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)(x-x_{0})
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) \big(i(y-y_{0})\big) \\
  & \phantom{=}
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big)(x-x_{0})
    + \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)\big(i(y-y_{0})\big) + \varepsilon(x,y) \\
  & = \Big[ \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) \Big] (x-x_{0}) \\
  & \phantom{=} 
    + \Big[ \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) \Big] \big(i(y-y_{0})\big)
    + \varepsilon(x,y)
\end{align*}
which can be written as
\begin{align*}
  f(z)-f(z_{0})
  & = \Big[ \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) \Big] \Big[ (x-x_{0}) +i(y-y_{0})\Big]
    + \varepsilon(x,y) \\
  & = \Big[ \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) \Big] (z-z_{0})
    + \varepsilon(x,y),
\end{align*}
where
\begin{align*}
  \varepsilon(x,y) 
  & =\epsilon_{5}(x,y)+\epsilon_{6}(x,y) \\
  & = \big(\epsilon_{1}(x,y) + i\epsilon_{3}(x,y)\big)(x-x_{0})
    + \big(\epsilon_{2}(x,y) + i\epsilon_{4}(x,y)\big)(y-y_{0}).
\end{align*}

Clearly then
\begin{align*}
  \frac{f(z)-f(z_{0})}{z-z_{0}}
  & = \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big)
    + \frac{\varepsilon(x,y)}{z-z_{0}},
  & & \mathrm{(V)}
\end{align*}
and the result will follow if we can show that the last term in the expression
above tends to 0 as $z-z_{0}$ tends to 0.

By the triangle inequality
\begin{align*}
  \Big|\frac{\varepsilon(x)}{(z-z_{0})}\Big|
  & \le \frac{|\big(\epsilon_{1}(x,y)+i\epsilon_{3}(x,y)\big)(x-x_{0})|
    +|\big(\epsilon_{2}(x,y)+i\epsilon_{4}(x,y)\big)(y-y_{0})|}{|z-z_{0}|} \\
  & \le \big(|\epsilon_{1}(x,y)|+|\epsilon_{3}(x,y)|\big) \frac{|x-x_{0}|}{|z-z_{0}|}
    +\big(|\epsilon_{2}(x,y)|+|\epsilon_{4}(x,y)|\big)\frac{|y-y_{0}|}{|z-z_{0}|}.
\end{align*}

Next, notice that $x-x_{0} = \mathbf{Re}(z-z_{0})$ and hence $|x-x_{0}| \le
|z-z_{0}|$; which leads to
$$\frac{|x-x_{0}|}{|z-z_{0}|} \le 1.$$

A similar argumentation for $y-y_{0} = \mathbf{Im}(z-z_{0})$ leads to
$$\frac{|y-y_{0}|}{|z-z_{0}|} \le 1.$$

We then have
\begin{align*}
  \Big|\frac{\varepsilon(x)}{(z-z_{0})}\Big|
  & \le |\epsilon_{1}(x,y)| + |\epsilon_{2}(x,y)|
    + |\epsilon_{3}(x,y)| + |\epsilon_{4}(x,y)|,
\end{align*}
and we conclude that $\varepsilon(x,y)/(z-z_{0})$ tends to 0 as $z$ tends to
$z_{0}$. Finally, letting $z$ approach $z_{0}$ in $\mathrm{(V)}$ leads to
\begin{align*}
  f'(z_{0})
  & = \lim_{z\to z_{0}}\frac{f(z)-f(z_{0})}{z-z_{0}} \\
  & = \lim_{z\to z_{0}} \Big(\frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big) \Big)
    + \lim _{z\to z_{0}} \frac{\varepsilon(x,y)}{z-z_{0}} \\
  & = \frac{\partial u}{\partial x}\big(x_{0},y_{0}\big)
    + i\frac{\partial v}{\partial x}\big(x_{0},y_{0}\big),
\end{align*}
which shows that $f$ is differentiable at $z_{0}$.

---

[Click here to access a `.pdf` version of this handout][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

