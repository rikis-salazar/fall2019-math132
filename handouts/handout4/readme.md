# _Cauchy's integral formula for the derivative of a function_

> I did not prove this result during class, but I did indicate the basic ideas.
> This document should help fill in the details that were left out during
> lecture.

## The statement of the theorem

Let $g$ be a continuous function on the contour $\Gamma$. For each $z$ not on
$\Gamma$ set
$$G(z):=\int_{\Gamma} \frac{g(\zeta)}{\zeta - z}\,\mathrm{d}z.$$
Then the function $G$ is analytic at each point not on $\Gamma$. Moreover its
derivative is given by
$$G'(z):=\int_{\Gamma} \frac{g(\zeta)}{(\zeta - z)^{2}}\,\mathrm{d}z.$$

> _Notes:_
>
> i.  this statement only requires continuity of $g$ (as opposed to analyticity); and
> ii. $\Gamma$ is not assumed to be a closed contour. 


## The _wrong_ proof

During lecture I mentioned that we can "verify" this identity by a process
known as _differentiation under the integral sign_. In symbols:
$$G'(z):=\frac{\mathrm{d}}{\mathrm{d}z}\Big(
  \int_{\Gamma} \frac{g(\zeta)}{\zeta - z}\,\mathrm{d}z \Big) 
  = \int_{\Gamma} \frac{\partial}{\partial z}
     \Big( \frac{g(\zeta)}{\zeta - z} \Big)\,\mathrm{d}z
   = \int_{\Gamma} \frac{g(\zeta)}{(\zeta - z)^{2}}\,\mathrm{d}z.$$

The basic problem is that in general, differentiation under the integral sign
only works in very specific circumstances. Later during the quarter we will show
that it does work in this particular setup, but in order to prove it we will
need... yes, you guessed it: _the theorem above_ (_i.e.,_ circular reasoning at
its finest).


## The _correct_ proof

This pretty much follows other arguments we have seen before. First we analyze
the difference $G(z+\Delta z) - G(z)$, then we make sure the term we want shows
its face, and finally we estimate the left over terms.

We have
\begin{align*}
  G(z+\Delta z) - G(z) 
  & = \int_{\Gamma} \frac{g(\zeta)}{\zeta - (z+\Delta z)}\,\mathrm{d}z
  - \int_{\Gamma} \frac{g(\zeta)}{\zeta - z}\,\mathrm{d}z \\
  & = \int_{\Gamma} g(\zeta)\Big( \frac{1}{\zeta - (z+\Delta z)}
      - \frac{1}{\zeta - z} \Big) \,\mathrm{d}z \\
  & = \int_{\Gamma} g(\zeta)
        \frac{\zeta -z -\zeta + z + \Delta z}
             {\big(\zeta - (z+\Delta z)\big)(\zeta-z)}
      \,\mathrm{d}z \\
  & = \int_{\Gamma}
        \frac{g(\zeta)\Delta z}{\big(\zeta - (z+\Delta z)\big)(\zeta-z)}
      \,\mathrm{d}z,
\end{align*}
which leads to
$$\frac{ G(z+\Delta z) - G(z) }{\Delta z} 
  = \int_{\Gamma}
      \frac{g(\zeta)}{\big(\zeta - (z+\Delta z)\big)(\zeta-z)}
    \,\mathrm{d}z.$$
To make sure the term we want _shows its face_, we simply add it and subtract it
to the right hand side of the expression above. This leads to
\begin{align*}
  \frac{ G(z+\Delta z) - G(z) }{\Delta z} 
  & = \int_{\Gamma} \frac{g(\zeta)}{(\zeta - z)^{2}}\,\mathrm{d}z
    + \int_{\Gamma}
        \frac{g(\zeta)}{\big(\zeta - (z+\Delta z)\big)(\zeta-z)}
      \,\mathrm{d}z
    - \int_{\Gamma} \frac{g(\zeta)}{(\zeta - z)^{2}}\,\mathrm{d}z \\
  & = \int_{\Gamma} \frac{g(\zeta)}{(\zeta - z)^{2}}\,\mathrm{d}z
    + \int_{\Gamma} \frac{g(\zeta)}{(\zeta - z)}\Big(
        \frac{1}{\zeta - (z+\Delta z)}
    - \frac{1}{\zeta - z} \Big) \,\mathrm{d}z \\
  & = \int_{\Gamma} \frac{g(\zeta)}{(\zeta - z)^{2}}\,\mathrm{d}z
    + \int_{\Gamma}
        \frac{g(\zeta)\Delta z}{\big(\zeta - (z+\Delta z)\big)(\zeta-z)^{2}}
      \,\mathrm{d}z.\\
\end{align*}

For the next step, we need some lower bounds on the polynomial terms $\zeta - z$
as well as $\zeta -(z + \Delta z)$. The trick is to make sure that both, $z$ as
well as $z+\Delta z$ are _far away enough_ from $\Gamma$. To this end, since by
assumption $z$ is not a point in $\Gamma$ we let $d>0$ be the distance[^one]
from $z$ to $\Gamma$; it then follows that $|z-\zeta| \ge d$, or
equivalently $1/|z-\zeta| \le 1/d$, holds for any $\zeta \in \Gamma$.
The remaining term is controlled by asking $\Delta z$ to remain close enough to
$z$ (which is already _far away_ from $\Gamma$).

By the triangle inequality, if $|\Delta z| < d/2$, we have for $\zeta \in \Gamma$
$$|\zeta - (z + \Delta z)| = |(\zeta - z) - \Delta z| \ge |\zeta-z| - |\Delta z|
  \ge d - |\Delta z| > d - \frac{d}{2} = \frac{d}{2}.$$

It then follows that
$$ \Big| \int_{\Gamma}
      \frac{g(\zeta)\Delta z}{\big(\zeta - (z+\Delta z)\big)(\zeta-z)^{2}}
    \,\mathrm{d}z \Big|
    \le \frac{|\Delta z| \ell(\Gamma)}{\frac{d}{2}\cdot d^{2}}
    \max_{\zeta\in\Gamma}|g(\zeta)|
    = \frac{2M\ell(\Gamma)}{d^{3}}|\Delta z|,$$
where $M$ is the maximum value of $|g(\zeta)|$ over $\Gamma$, and $\ell(\Gamma)$
denotes the length of $\Gamma$. Which shows that
$$\lim_{\Delta z\to0} \int_{\Gamma}
      \frac{g(\zeta)\Delta z}{\big(\zeta - (z+\Delta z)\big)(\zeta-z)^{2}}
    \,\mathrm{d}z  = 0,$$
and hence
\begin{align*}
  G'(z) & = \lim_{\Delta z\to0} \frac{ G(z+\Delta z) - G(z) }{\Delta z} \\
        & = \lim_{\Delta z\to 0}
      \int_{\Gamma} \frac{g(\zeta)}{(\zeta - z)^{2}}\,\mathrm{d}z
    + \lim_{\Delta z\to 0}
    \int_{\Gamma}
        \frac{g(\zeta)\Delta z}{\big(\zeta - (z+\Delta z)\big)(\zeta-z)^{2}}
      \,\mathrm{d}z \\
        & = \int_{\Gamma} \frac{g(\zeta)}{(\zeta - z)^{2}}\,\mathrm{d}z.
\end{align*}




[^one]: Here one needs to carefully argue you why $d$ exists and is positive.
  The existence follows from the fact the it is obtained via an infimum of a
  non-empty set of real numbers. On the other hand the fact that $\Gamma$ is the
  image of a continuous function (say from  $[a,b]\subset\mathbb{R}$ onto
  $\Gamma$) will guarantee that this infimum is not zero.




---

[Click here to access a `.pdf` version of this handout][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

