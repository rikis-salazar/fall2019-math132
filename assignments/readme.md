# Homework assignments

Homework assignment problems fall into two categories: _regular_, and
_miscellaneous_ exercises.

*   _Regular_ exercises are the ones you are supposed to hand in during lecture.
    Your TA will select and grade only three of these problems.
*   _Miscellaneous_ exercises are for you to practice key concepts/tools we
    develop during lecture, and/or to help you further explore
    ideas/models/solutions that were briefly discussed during lecture.

    > These problems will not be collected (nor graded), but you are more than
    > welcome to ask me or your TA about them during office hours.

---

## Homework 1

_Status:_ ~~In progress.~~ Final.  
**Due date:** Monday, October ~~6~~ 7.

> No additional exercises will be added to this list.

From Saff & Snider, _Fundamentals of Complex Analysis_.

-   _Regular_ exercises:
    *   _Section 1.1:_ 6, 8, 10, 12, 14, 20, 22, 28.
    *   _Section 1.2:_ 7 a) d) e), 16, 17.
    *   _Section 1.3:_ 2, 4, 8,  12, 14, 20, 22, 28.

-   _Miscellaneous_ exercises:
    *   _Section 1.1:_ 26, 27, 29.
    *   _Section 1.2:_ 19.
    *   _Section 1.3:_ 20, 21, 22.

From [Beck, et al., _A Firt Course in COMPLEX ANALYSIS_.][book2]  

-   _Regular_ exercises:
    *   _Chapter 1 (pp. 13--17):_ 1.1 a) d), 1.2 a) b), 1.3 a) c), and 1.4 a) b) f).

-   _Miscellaneous_ exercises:
    *   _Chapter 1:_ 1.7, 1.9, 1.10, 1.13, 1.19, 1.23 a) f), and 1.24 a).

---

## Homework 2

_Status:_ Final.  
**Due date:** Monday, October 14.

> No additional exercises will be added to this list.

From Saff & Snider, _Fundamentals of Complex Analysis_.

-   _Regular_ exercises:
    *   _Section 1.4:_ 2, 4, 8, 12 b), 16, 20, 22.

    > **Note:** For these exercises, use definition number 5, on page 27.
    > Namely
    > $$\mathrm{e}^{x+iy} := \mathrm{e}^{x}(\cos y + i \sin y).$$

    *   _Section 1.5:_ 5 a) e), 7 a) c), 9 (_Hint:_ What happens when $z=1$?),
        11 (_Hints:_ Divide both sides of the equation by $z^{5}$. Also, notice
        that the equation is of degree 4. Why?).
    *   _Section 1.7:_ 1, 5.
    *   _Section 2.1:_ 1 a) d) e), 4, 8, 9.
    *   _Section 2.2:_ 3, 4, 5. 7 a) b) d).


-   _Miscellaneous_ exercises:
    *   _Section 1.4:_ 11, 19, 21, 23.
    *   Use the equations
        $$\cos\theta = \mathbf{Re}( \mathrm{e}^{i\theta} ) =
        \frac{\mathrm{e}^{i\theta}+\mathrm{e}^{-i\theta}}{2},\qquad
        \sin\theta = \mathbf{Im}( \mathrm{e}^{i\theta} ) =
        \frac{\mathrm{e}^{i\theta}-\mathrm{e}^{-i\theta}}{2i},$$
        to prove the following trigonometric identities
        a)  $\sin^{2}\theta + \cos^{2}\theta = 1$
        a)  $\cos(\theta_{1}+\theta_{2}) = \cos\theta_{1}\cos\theta_{2} -
            \sin\theta_{1}\sin\theta_{2}$
    *   Let $\mathbf{N}$ denote the _North Pole_ of the unit sphere in the
        3-dimentional space. That is, let $\mathbf{N} = (0,0,1)^{T}$.
        a)  Show that the line that passes throught $\mathbf{N}$ and the point
            $\mathbf{P}=(p_{1},p_{2},p_{3})^{T}$ in the unit sphere, can be
            parametrized by $\ell(t) = (p_{1}t,p_{2}t,p_{3}t+1-t)^{T}$.
        b)  Use the above parametrization to derive the inverse stereographhic
            projection formula
            $$(p_{1},p_{2},p_{3})^{T} \to \frac{p_{1}}{1-p_{3}} +
            i\frac{p_{2}}{1-p_{3}}.$$
            _Hint:_ Study the intersection of the graph of $\ell(t)$ and the
            $p_{1}p_{2}$-plane.
    *   Show that _all_ lines and circles in the complex plane correspond under
        stereographic projection to circles on the Riemann sphere.[^one]
    *   _Section 2.1:_ 12.
    *   _Section 2.2:_ 15, 16.


---

## Homework 3

_Status:_ Final.  
**Due date:** Wednesday, October 23.

> No additional exercises will be added to this list.

From Saff & Snider, _Fundamentals of Complex Analysis_.

-   _Regular_ exercises:
    *   _Section 2.2:_ 1, 2, 11 a) d) f), 14, 15.

        > **Note:** In problem 11 d), the answer is the _point at infinity_. It
        > is not unlikely that the authors intended to have readers compute a
        > slightly different exercise; namely, to compute the limit
        >
        > $$\lim_{z\to i} \frac{z^{2}+1}{z^{4}-1}.$$

    *   _Section 2.3:_ 3, 7 b) c), 14.

        > _Hint for problem 3:_ If $z\neq z_{0}$,
        >
        > $$f(z) = f(z_{0}) + \frac{f(z)-f(z_{0})}{z-z_{0}} (z-z_{0}).$$

    *   _Section 2.4:_ 3, 5, 7, 11.
    *   _Section 2.5:_ 1 a) c), 3 a) b) c), 5, 9.
    *   _Section 3.2:_ 1, 5 a) c) d), 9 a) b), 11, 17 a) b).
    *   _Section 3.3:_ 1 a) d), 3, 5, 8.


-   _Miscellaneous_ exercises:
    *   _Section 2.3:_ 2, 16.

        > _Hint for problem 2:_ If $z\neq z_{0}$,
        >
        > \begin{align}
        >   f(z) & = f(z) + f(z_{0}) + f'(z_{0})(z-z_{0}) - f(z_{0}) - f'(z_{0})(z-z_{0}) \\
        >        & = f(z_{0}) + f'(z_{0})(z-z_{0}) + \frac{f(z)-f(z_{0})}{z-z_{0}}(z-z_{0}) - f'(z_{0})(z-z_{0}).
        > \end{align}

    *   _Section 2.4:_ 6, 12.
    *   _Section 2.5:_ 6, 8 a) c).
    *   _Section 3.2:_ 3, 23.
    *   _Section 3.3:_ 6.

---

## Homework 4

_Status:_ Final.  
**Due date:** Monday, October 28.

1.  Find the domain of analiticity of the function
    $$f(z) = z \sqrt{1-\frac{1}{z^{2}}},$$
    where the square root symbol denotes the principal branch of the power
    function with exponent $\alpha=1/2$.

    > _Note:_ The solution set is pictured in figure 3.18 (page 134) of your
    > textbook; however, your job is to argue you why such representation is
    > accurate.

1.  Find all the solutions of the equation $(z-1)^{2019} = z^{2019}$.

    > _Hint:_ There are exactly 2018 solutions; none of them are real. See also
    > problem 11 in section 1.5 and its hint above (in the homework 2 section).

1.  Let $\partial D_{2}$ denote the _boundary_ of the open disk with radius 2.
    This is

    $$\partial D_{2} = \partial\{ z\in\mathbb{C}\,:\, |z| < 2 \} 
      = \{ z\in\mathbb{C}\,:\, |z| = 2 \}.$$

    i.  Explain why on this set, the inequality

        $$\Big| \frac{1}{z^{2}-1} \Big| \le \frac{1}{3}$$

        holds.

    i.  Does the above inequality hold in the set $D_{2} = \{
        z\in\mathbb{C}\,:\, |z| < 2 \}$? If the answer is yes, prove it; if it
        is no, explain why not.

    > _Hint:_ Use the _often forgotten_ part of the triangle inequality, namely
    > $\big| |a| - |b| \big| \le |a-b|$.

1.  Suppose $f$ is entire, with real and imaginary parts $u$ and $v$ satisfying

    $$u(x,y)v(x,y) = 2019$$

    for all $z=x+iy$. Show that $f$ must be constant.

    > _Hint:_ Note that $u$ and $v$ cannot be zero regardless of $x$ and $y$; in
    > particular $u^{2}+v^{2}$ is never zero. Then show that $u_{x} = v_{x} = 0$.

1.  Prove the following _weak version_ of _L'H&ocirc;pital's rule_: if the
    complex-valued functions $f(z)$ and $g(z)$ are analytic at $z_{0}$ and
    $f(z_{0}) = g(z_{0}) = 0$, but $g'(z_{0}) \neq 0$ and $g(z)$ is never zero
    in the _punctured disk_ $\{z\,:\, 0 < |z-z_{0}| < \delta \}$ for some $\delta
    >0$, then

    $$\lim_{z\to z_{0}} \frac{f(z)}{g(z)} = \frac{f'(z_{0})}{g'(z_{0})}.$$

    > _Hint:_ $f(z) = f(z)-f(z_{0})$, and $g(z) = g(z)-g(z_{0})$.

1.  Let $\mathcal{D}= \{ x + iy \,:\, x\neq 0,\, y\neq 0 \}$. Is the function

    $$\varphi(x,y) = \mathrm{e}^{\frac{y}{x^{2}+y^{2}}}
      \sin\left(\frac{x}{x^{2}+y^{2}}\right)$$

    harmonic in $\mathcal{D}$? Justify your answer.

    > _Hint:_ Study the function $\mathrm{e}^{i/z}$.


## Homework 5

_Status:_ ~~In progress~~ Final.  
**Due date:** Wednesday, November 6.

> Additional exercises will not be added to this list.

From Saff & Snider, _Fundamentals of Complex Analysis_.

-   _Regular_ exercises:
    *   _Section 4.1:_ 1 a) b), 3, 7, 10 b).
    *   _Section 4.2:_ 3 a) b), 6 a) b), 7, 14 a), 16.
    *   _Section 4.3:_ 1 a) b) c) f), 2, 3 a), 10.

1.  Compute the integral

    $$\int_{\Gamma} z^{2}\,\mathrm{d}z$$

    along the simple closed contour $\Gamma$ pictured below.

    | ![fig1.png](fig1.png) |
    |:---:|
    |Figure 1: the contour &Gamma;. |

1.  Let $\Gamma$ denote the closed contour with corners $\alpha_{1} = 1$,
    $\alpha_{2} = i$, $\alpha_{3} = -1$, $\alpha_{4} = -i$, traversed in a
    _clockwise_ direction. Compute the integral

    $$\int_{\Gamma} \frac{1}{z}\,\mathrm{d}z.$$

    Does it matter if $\Gamma$ is traversed _counterclockwise_?


## Homework 6

_Status:_ Final.  
**Due date:** Wednesday, November 27.

1.  Suppose $f$ and $g$ are analytic in the simply connected domain $D$. If
    $\Gamma$ is a simple closed contour completely contained in $D$, and if
    $f(z) = g(z)$ for all $z$ along $\Gamma$, show that $f(z) = g(z)$ for every
    point inside of $\Gamma$.

    > _Hint:_ Let $a$ be a point inside $\Gamma$. Apply Cauchy's integral
    > formula to compute the value $h(a)$, where $h$ is the difference function
    > $h=f-g$.

1.  Evaluate
    $$\int_{\Gamma} \bar{z}\,\mathrm{d}z,$$
    where $\Gamma$ is the arc of the parabola $y=x^{2}$ from $x=0$ to $x=1$.

1.  Integrate the following functions over the set $|z|=1$, traversed in the
    positive direction:
    
    i. $\dfrac{\mathrm{e}^{2z}}{4z-i\pi}$
    i. $\dfrac{13z+2}{(z^{2}-4)^{2}}$

1.  Let $$f(z) =\frac{\mathrm{e}^{iz}}{1+z^{2}}.$$

    i.  Show that if $R>1$, the integral $$\int_{\sigma_{R}} f(z)\,\mathrm{d}z,$$
        equals $\pi/\mathrm{e}$; where $\sigma_{R}$ is the counterclockwise
        semicircle formed by the segment $[-R,R]$ on the real axis, followed by
        the circular arc of radius $R$ in the upper half plane from $R$ to $-R$.

    i.  Show that if $|z|$ is sufficiently large and $z$ is in the _upper half_
        plane, then
        $$|f(z)| \le \frac{C}{|z|^{2}},$$
        where $C$ is a positive constant. Then show that
        $$\lim_{R\to\infty}\int_{\gamma_{R}} f(z)\,\mathrm{d}z = 0,$$
        where $\gamma_{R}$ is the circular arc of radius $R$ in the upper half
        plane from $R$ to $-R$.

    i.  By considering the real part of $f$ and by taking an appropriate limit,
        carefully explain why
        $$\int_{-\infty}^{\infty} \frac{\cos x}{1+x^{2}}\,\mathrm{d}x
          = \frac{\pi}{\mathrm{e}}.$$

1.  Let $f$ be an entire function that is _bounded_ by 2019. That is, $f$ is
    analytic in $\mathbb{C}$, and $|f(z)| \le 2019$ for all $z\in\mathbb{C}$.
    Prove that $f$ must be a constant function.

    > _Hint:_ For an arbitrary point $a\in\mathbb{C}$, integrate a carefully
    > selected function over the set $|z-a| = R$ to obtain the estimate
    > $|f'(a)|\le \frac{2019}{R}$.

1.  Let $f(z) = \mathrm{Log}\,z$, denote the principal branch of the complex
    logarithm function; and let $g(z) = \mathcal{L}_{0}(z)$, denote the branch
    of the complex logarithm with cut along the non-negative part of the real
    axis.  That is
    $$\mathcal{L}_{0}(z) = \mathrm{Log}|z| + i\mathrm{arg}(z),\quad 0 <
    \mathrm{arg}(z) \le 2\pi.$$
    Use problem 1 above to prove that $f = g$ on the set $\mathbf{Im}(z) > 0$.

    > _Hint:_ For arbitrary $r_{2}>r_{1}>0$, and $0< \theta_{1} < \theta_{2} <
    > \pi$, consider the set $\{z\,:\,\mathrm{e}^{r_{1}} \le |z| \le
    > e^{r_{2}},\, \theta_{1} \le \mathcal{L}_{0}(z) \le \theta_{2} \}$. Carefully
    > show that $f=g$ on the boundary of this set.


## Homework 7

_Status:_ ~~In progress~~ Final.  

**Due date:** Friday, December 6.

> Additional exercises will be added to this list.

From Saff & Snider, _Fundamentals of Complex Analysis_.

-   _Regular_ exercises:
    *   _Section 4.5:_ 8, 11.

        > _Note for problem 11:_
        > 
        > We _proved_ this a while back under the extra assumption that the
        > derivatives ($u_{x}$, $u_{y}$, $v_{x}$, and $v_{y}$) were continuous.

    *   _Section 4.6:_ 1, 3, 5, 6, 7, 13, and 14.
    *   _Section 5.2:_ 1 a) b) d), 5 a) b), 14, 15.
    *   _Section 5.3:_ 7, 8, 9.
    *   _Section 5.5:_ 1 a) b), 3 a) c).


-   _Miscellaneous_ exercises:
    *   _Section 5.1:_ 16.
    *   _Section 5.2:_ 8 a) b) c).
    *   _Section 5.3:_ 15 a) b).
    *   _Section 5.5:_ 11.

---


[Return to main course website][MAIN]


[book2]: http://math.sfsu.edu/beck/complex.html
[MAIN]: ..
[^one]: The _Riemann sphere_ is a fancy way to call the set $S_{2} = \{ (p_{1},p_{2},p_{3})^{T} \, : \, p_{1}^{2} + p_{2}^{2} + p_{3}^{2} = 1 \}$.




