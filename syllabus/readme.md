# Syllabus: Fall 2019 Math 132 Lecture 2

> If you are reading the online (html) version of this syllabus, [click
> here][pdf] to access a `.pdf` version of this document.[^if-you]

[pdf]: readme.pdf
[^if-you]: The embedded link does not work in the `.pdf` document.

---

## Course information

**Contact:** [rsalazar@math.ucla.edu][correo] (write "Math 132" in the subject).

**Time:** 11:00 to 11:50 am.

**Location:** MS 6229

**Office hours:** [See CCLE class website (CCLE)][class-website].

**Teaching assistant(s):**

|**Section**| **T. A.**       |**Office**| **E-mail**                            |
|:---------:|:----------------|:--------:|:--------------------------------------|
| 2A        | Stone, Bertrand | MS 2905  | [`bertrand.stone@math.ucla.edu`][t-a] |

[correo]: mailto:rsalazar@math.ucla.edu
[class-website]: https://ccle.ucla.edu/course/view/19F-MATH132-2?section=0
[t-a]: mailto:bertrand.stone@math.ucla.edu

## Course Description

Introduction to basic formulas and calculation procedures of complex analysis of
one variable relevant to applications. Topics include: _Cauchy/Riemann_
equations, _Cauchy_ integral formula, power series expansion, contour integrals,
residue calculus.


## Textbook & supplemental references

*   Saff, E. B. & Snider, A. D., _Fundamentals of Complex Analysis with
    Applications to Engineering and Science_, Third Edition, Pearson Education,
    Inc.

    > Please note that this textbook was selected by the instructor who was
    > originally appointed to teach this course. I will do my best to use it as
    > our _main_ reference, however I will use some of the textbooks below to
    > present some topics during lecture, and/or to assign homework exercises.

*   Beck, M. et al., _A First Course in COMPLEX ANALYSIS_, Version `1.54`.

    > "The most current version of this book is available at the website
    > [`http://math.sfsu.edu/beck/complex.html.`][book2] This book may be freely
    > reproduced and distributed, provided that it is reproduced in its
    > entirety from the most recent version."

*   Gamelin, T., _Complex Analysis_, Springer/Verlag.

    > This is the textbook that the [course outline][outline] is based upon. It
    > should be _on reserve_ at the UCLA SEL Library (Boelter Hall 8th floor).

[book2]: http://math.sfsu.edu/beck/complex.html


## Course outline

We will attempt to cover the material in the [mathematics department general
course outline][outline]. Although an effort will be made to follow these
guidelines, material presented during lecture might be chosen from supplemental
references and/or material readily available online.

[outline]: https://www.math.ucla.edu/ugrad/courses/math/132


## CCLE and MyUCLA

This course will use a combination of a password-protected Internet site (CCLE),
as well as other regular (_i.e.,_ non-protected) sites to post course materials
and announcements. These materials can include the syllabus, handouts and
Internet links referenced in class. On some special occasions hard copies of
said materials will be available during class, however these occasions will
constitute the exception as opposed to the rule; that is, in most cases hard
copies of these materials **will not** be made available to the class.


## Reaching me via email

_During this current quarter I will be in charge of course(s) were enrollment is
higher than usual for a UCLA class._ In practice, this means that emails you
send to my email address might go unanswered for a rather long period of time.
Before sending me a message, you are encouraged to consult CCLE, as well as this
syllabus, as your question(s) might already be answered there.

If you feel that your question/issue has not been addressed in the documents
listed above, do feel free to send me a message, however keep the following in
mind:

*   I receive a high volume of messages throughout the day; in most cases it
    will be faster for you to get the information you seek if you reach out to
    me in person (say during O.H., or right before/after lecture).
*   Messages with special keywords _skip_ my inbox. Use this to your advantage:
    if you add `Math 132` to your subject line, your message will find its way
    into a special folder that I check periodically. In most cases this reduces
    the time you have to wait before I reply to it.
*   Messages with special attachments, more specifically text files with
    specific file extensions (_e.g.,_ `.cpp`, `.h`, etc.), as well as messages
    containing some types of image attachments (_e.g.,_ `png`, `jpg`, `pdf`,
    etc.), are sent directly to my _trash_ folder.

    > This inconvenience brought to you by students that are under the
    > impression that deadlines do not apply to them (_e.g.,_ they attempt to
    > _late-submit_ their assignments via email attachments).

## Grading

_Grading method:_ pop quizzes (**Quiz**), homework assignments (**Hw**), midterms
(**Mid1**, **Mid2**), and final exam (**Final**).

*   _Pop quizzes:_ there will be **at least 10 pop quizzes** throughout the quarter.
    Each quizz will be worth **up to 2 full percentage points** in the
    computation of your overall final score. **There will be no advanced
    notice** of when these quizzes will take place, but in principle, you can
    miss up to 5 of them and still qualify for a 100% overall score (see grading
    schemas below).

    > Note: the maximum that can be earned in this category is 10%. That is, if
    > you answer correctly 6 or more of these quizzes, you can only be awarded
    > the maximum score of 10% as opposed to 12% or more.

*   _Homework assignments:_ homework will be assigned on a weekly basis
    throughout the quarter. **No late homework will be accepted** for any
    reason, but **your lowest 2 homework scores will not count** towards the
    computation of your overall score.

*   _Midterm:_ two fifty-minute midterms will be given on **October 21 (Monday
    week 4)**, and **November 18 (Monday week 8)** from **11:00 to 11:50 am** at **MS
    6229**.

*   _Final exam:_ this exam will be given on **Monday, December 9** from **3:00
    to 6:00 pm** at **TBA**.  

    [Click here for up-to-date information about the exam location][final-tba].  

    > **Important:**  
    > 
    > _Failure to take the final exam during this time will result in an
    > automatic F!_

[final-tba]: https://sa.ucla.edu/ro/public/soc/Results/ClassDetail?term_cd=19F&subj_area_cd=MATH&crs_catlg_no=0132++++&class_id=262469210&class_no=+002++

Your final score in the class will be the maximum of the following two grading
breakdowns:

|                                                                        |  
|:----------------------------------------------------------------------:|  
| 10% **Quiz** + 10% **Hw** + 25% **Mid1** + 25% **Mid2** + 30% **Final** |  
| or |  
| 10% **Quiz** + 10% **Hw** + 30% **Highest Midterm** + 50% **Final** |  


Overall scores determine letter grades according to the table below.

|                       |                      |                    |  
|:----------------------|:---------------------|:-------------------|  
| A+ (N/A)              | A (93.33% or higher) | A- (90% -- 93.32%) |  
| B+ (86.66% -- 89.99%) | B (83.33% -- 86.65%) | B- (80% -- 83.32%) |  
| C+ (76.66% -- 79.99%) | C (73.33% -- 76.65%) | C- (70% -- 73.32%) |  

The remaining grades will be assigned at my own discretion. Please do not
request exceptions.

> **Note:** students taking this class on a P/NP basis that attain an
> overall score in the C- range **are not** guaranteed a P letter grade.

**All grades are final when filed by the instructor on the Final Grade Report.**


### Policies and procedures (exams)

Make sure to bring your UCLA ID card to every exam. You will not be allowed to
consult books, notes, the internet, digital media or another student's exam;
however you may be allowed to use a _cheat sheet[^card]_. Please turn off and put away
any electronic devices during the entire duration of the exam.

**There will be absolutely no makeup midterms under any circumstances.**
However, notice that this class **features a dual grading schema** for the
purposes of grade computations. This makes it possible for students to earn a
100% overall final score even they miss one midterm exam.

Exams will be returned to you during discussion section and your TA will go over
the exam at that time. _Any questions regarding how the exam was graded **must
be submitted in writing** with your exam to the TA at the end of section that
day._ No regrade requests will be allowed afterwards regardless of whether or
not you attended section. Please get in touch with me if you anticipate missing
section due to a family emergency or a medical reason.


[^card]: A two-sided `4x6` inch card might be allowed on specific exams.
  However, this shall be the exception, not the rule.


### Policies and procedures (assignments)

Homework will be assigned on a weekly basis, and it will be collected the
following week (Monday). No late homework will be accepted for any reason, but
as pointed out elsewhere in this document, your 2 lowest scores will be dropped.

Scores on homework assignments, quizzes, and exams will appear on
[MyUCLA][my-ucla] before these items are returned to you. It is your
responsibility to verify in a timely manner that the scores appearing therein
are accurate.

[my-ucla]: https://my.ucla.edu/


## Accommodations

If you need any accommodations for a disability, please contact the UCLA
CAE[^two] [(`www.cae.ucla.edu`)][CAE]. Make sure to let me know as soon as
possible about necessary arrangements that need to be made.

[CAE]: https://www.cae.ucla.edu
[^two]: Center for Accessible Education

|                                                      |
|:----------------------------------------------------:|
| Course Syllabus Subject to Update by the Instructor. |

