# Practice material

Unfortunately, I do not have _old exams_ that I can release at this point.
Instead, here you will find a collection of exercises that should represent the
degree of difficulty, as well as the course topics that you can expect in our
midterms.

Please note that, as the name of this section implies, exercises posted here are
to be regarded as practice material. There is no guarantee the midterm problems
will look like the ones listed below.

---

## Practice for final exam

_Status:_ Final.

> Additional exercises will not be added to this list.

1.  Assume $u$ is _harmonic_ in a domain $G$, and assume that for a given $w\in
    G$ the set $\{z\in\mathbb{C}\,:\, |z-w|\le r \}$ is completely contained in
    $G$ for some $r>0$.

    Prove that
    $$u(w)=\frac{1}{2\pi}\int_{0}^{2\pi} u(w+r\mathrm{e}^{it})\,\mathrm{d}t.$$

    In other words, the value of a harmonic function at the center of a disk is
    the average over the values at the boundary of the disk.

    > _Hint:_ A similar result holds for analytic functions. Construct $f$
    > analytic such that $u = \mathbf{Re}(f)$.

    > **Solution:**
    >
    > Recall that given a harmonic function $u$, it is possible to construct a
    > function $v$ such that $f = u + iv$ is analytic. Let $f$ be such a
    > function, then for $w\in G$
    > $$f(w)=\frac{1}{2\pi}\int_{0}^{2\pi} f(w+r\mathrm{e}^{it})\,\mathrm{d}t.$$
    > The result then follows by taking the real part of the equation above.

1.  Compute the residue of
    $$\frac{\mathrm{e}^{2z}}{(z-1)^{2}(z-2)}$$
    at $z=1$, and $z=2$; then use this information to obtain the following
    integral
    $$\oint_{|z-3|=2019} \frac{\mathrm{e}^{2z}}{(z-1)^{2}(z-2)}\,\mathrm{d}z.$$

    > **Solution:**
    >
    > Let $f(z)=\frac{\mathrm{e}^{2z}}{(z-1)^{2}(z-2)}$. Since the pole at $z=1$
    > is of order two, we have
    > \begin{align*}
    >   \mathrm{Res}[f(z),z=1]
    >     & = \frac{1}{(2-1)!} \lim_{z\to 1}
    >         \frac{\mathrm{d}^{2-1}}{\mathrm{d}z^{2-1}}\Big( (z-1)^{2}f(z) \Big) \\
    >     & = (1) \lim_{z\to 1} \frac{\mathrm{d}}{\mathrm{d}z}
    >         \Big( \frac{\mathrm{e}^{2z}}{(z-2)} \Big) \\
    >     & = \lim_{z\to 1} \frac{\mathrm{e}^{2z}\big(2(z-2) - 1\big)}{(z-2)^{2}} \\
    >     & = -3\mathrm{e}^{2}.
    > \end{align*}
    > On the other hand, since the pole at $z=2$ is of order one, we have
    > \begin{align*}
    >   \mathrm{Res}[f(z),z=2] & = \lim_{z\to 2}(z-2)f(z) \\
    >     & = \lim_{z\to 2}\frac{\mathrm{e}^{2z}}{(z-1)^{2}} \\
    >     & = \frac{\mathrm{e}^{4}}{(1)^{2}} \\
    >     & = \mathrm{e}^{4}.
    > \end{align*}
    >
    > Therefore
    > $$\oint_{|z-3|=2019} \frac{\mathrm{e}^{2z}}{(z-1)^{2}(z-2)}\,\mathrm{d}z
    >   = 2\pi i \big( \mathrm{Res}[f(z),z=1] + \mathrm{Res}[f(z),z=2] \big)
    >   = 2\pi i \mathrm{e}^{2}(\mathrm{e}^{2} - 3).$$

1.  Suppose $f$ is entire and $\lim_{z\to\infty} f(z)$ is finite. Show that $f$
    must be constant.

    > _Hints:_
    >
    > i.  It suffices to show that $f$ is bounded. Why?
    > ii. Say $\lim_{z\to\infty}f(z) = L$; then take $\epsilon =1$ in the
    >     definition of the limit to conclude that $|f(z)|\le |L| + 1$, if
    >     $|z|>R$ for some positive $R$. What can you say about the values of $f$
    >     in the set $|z|\le R$?

    > **Solution:**
    >
    > By the definition of limit at infinity, given $\epsilon = 1$ we can find
    > $R$ positive and _big enough_ so that
    > $$|f(z)-L|<1\quad\text{whenever}\quad|z|>R,$$
    > we then have for $|z|>R$
    > $$|f(z)| = |f(z) - L + L| \le |f(z) - L| + |L| < 1 + |L|$$
    >
    > On the other hand, since $f$ is entire, it is also continuous in
    > $\mathbb{C}$. In turn, this means that the _real-valued_ function $|f|$ is
    > continuous for any $x=\mathbf{Re}(z)$ and any $y=\mathbf{Im}(z)$. In
    > particular $|f|$ is continuous in the set $\{(x,y)\in\mathbb{R}^{2}\,:\,
    > x^{2} + y^{2} \le R^{2}\}$ which is geometrically equivalent to the
    > complex disk $\{z\in\mathbb{C}\,:\, |z|\le R\}$. Since this latter set is
    > _closed_ and _bounded_, it follows that $|f|$ attains its maximum (say
    > $M$) [and its minimum (say $m$)] within the set.
    >
    > Summarizing:
    > i.  $|f(z)| \le 1 + |L|$ if $|z|>R$, and
    > ii. $|f(z)| \le M$, if $|z|\le R$.
    >
    > It follows then that $f$ is constant in $\mathbb{C}$ as $f$ is entire and
    > bounded.

1.  Suppose $f$ is entire and there exists $M$ positive such that $|f(z)| \ge M$
    for all $z\in\mathbb{C}$. Prove that $f$ is constant.

    > **Solution:**
    >
    > Since $|f(z)| > 0$ for all $z$, $f$ does not have zeros in $\mathbb{C}$.
    > Thus the function $\frac{1}{z}$ is also entire. Moreover $\frac{1}{f(z)}$
    > is bounded by $M^{-1}$. Therefore $1/f$ must be constant. This of course
    > is only possible if $f$ itself is constant.

1.  Take for granted that the first four terms of the _Laurent_ expansion of
    $\frac{1}{\sin z}$ around $z=0$ are
    $$\frac{1}{z},\quad\frac{1}{6}z,\quad\frac{7}{366}z^{3},\quad\frac{31}{15120}z^{5}.$$

    i.  Compute the integral
        $$\oint_{|z|=1} \frac{\mathrm{e}^{z}}{\sin z}\,\mathrm{d}z$$

    i.  Use the fact that $\sin z = \sin(\pi - z)$ to write the first four terms
        of the _Laurent_ expansion of $\frac{1}{\sin z}$ around $z=\pi$.

    i.  Use the identity
        $$\mathrm{e}^{z} =\mathrm{e}^{\pi} \mathrm{e}^{z-\pi}$$
        to compute the _Laurent_ series of $\mathrm{e}^{z}$ around $z=\pi$.

    i.  Use the results from _i.--iii._ above to compute the integral
        $$\oint_{|z-2| = 3} \frac{\mathrm{e}^{z}}{\sin z}\,\mathrm{d}z$$

    > **Solution:**
    >
    > i.  Since the _Laurent_ series of $\mathrm{e}^{z}$ centered at $z=0$ is
    >     $$ 1 + z + \frac{z^{2}}{2} + \frac{z^{3}}{6} + \cdots + \frac{z^{n}}{n!}\cdots $$
    >     we have
    >     \begin{align*}
    >       \frac{\mathrm{e}^{z}}{\sin z} = & \mathrm{e}^{z}\frac{1}{\sin z} \\
    >         = & \Big( 1 + z + \frac{z^{2}}{2} + \frac{z^{3}}{6} + \cdots + \frac{z^{n}}{n!}\cdots \Big)
    >             \Big( \frac{1}{z} + \frac{1}{6}z + \frac{7}{366}z^{3} + \frac{31}{15120}z^{5} + \cdots \Big) \\
    >         = & \Big( \frac{1}{z} + \frac{1}{6}z + \frac{7}{366}z^{3} + \frac{31}{15120}z^{5} + \cdots \Big)
    >           + \Big( 1 + \frac{1}{6}z^{2} + \frac{7}{366}z^{4} + \frac{31}{15120}z^{6} + \cdots \Big) \\
    >         & + \Big( \frac{1}{2}z + \frac{1}{12}z^{3} + \frac{7}{732}z^{5} + \frac{31}{30240}z^{7} + \cdots \Big)
    >           + \Big( \frac{1}{6}z^{2} + \frac{1}{36}z^{4} + \frac{7}{2196}z^{6} + \frac{31}{90720}z^{8} + \cdots \Big) + \cdots \\
    >         = & \frac{1}{z} + 1 + \Big(\frac{1}{6} + \frac{1}{2}\Big)z + \Big(\frac{1}{6} + \frac{1}{6} \Big)z^{2} +
    >             \Big( \frac{7}{366} + \frac{1}{12} \Big)z^{3}+ \cdots
    >     \end{align*}
    >    which shows that the residue at $z=0$ is equal to 1. Thus
    >    $$\oint_{|z|=1} \frac{\mathrm{e}^{z}}{\sin z}\,\mathrm{d}z = 2\pi i.$$
    >
    > ii. Since $\sin z = -\sin(\eta)$, where $\eta = z-\pi$, the first four
    >     terms of the _Laurent_ series of $\frac{1}{\sin z}$ centered at
    >     $z=\pi$ correspond to the negatives of the first four terms of
    >     $\frac{1}{\sin\eta}$ centered around $\eta = 0$. Hence
    >     $$\frac{1}{\sin z} = -\frac{1}{z-\pi} - \frac{1}{6}(z-\pi) - \frac{7}{366}(z-\pi)^{3} - \frac{31}{15120}(z-\pi)^{5} - \cdots$$
    >
    > iii. We have $\mathrm{e}^{z} = \mathrm{e}^{\pi}\mathrm{e}^{\eta}$, where
    >     as before, $\eta = z - \pi$. As in the previous part of this problem,
    >     expanding $\mathrm{e}^{\pi}\mathrm{e}^{\eta}$ around $\eta = 0$
    >     produces the desired expansion of $\mathrm{e}^{z}$ around $z = \pi$.
    >     This is
    >     $$\mathrm{e}^{z} =  \mathrm{e}^{\pi} + \mathrm{e}^{\pi}(z-\pi) +
    >     \frac{\mathrm{e}^{\pi}}{2}(z-\pi)^{2} +
    >     \frac{\mathrm{e}^{\pi}}{6}(z-\pi)^{3} +
    >     \cdots + \frac{\mathrm{e}^{\pi}}{n!}(z-\pi)^{n} + \cdots$$
    >
    > iv. A similar computation to part _i._ above leads to
    >     \begin{align*}
    >       \frac{\mathrm{e}^{z}}{\sin z} = & \mathrm{e}^{z}\frac{1}{\sin z} \\
    >         = & \mathrm{e}^{\pi}\Big( 1 + (z-\pi) + \frac{(z-\pi)^{2}}{2} + \cdots + \frac{(z-\pi)^{n}}{n!}\cdots \Big)
    >             \Big( -\frac{1}{z-\pi} - \frac{1}{6}(z-\pi) - \frac{7}{366}(z-\pi)^{3} - \cdots \Big) \\
    >         = & \dots \text{ and a miracle occurs } \dots \\
    >         = & -\frac{\mathrm{e}^{\pi}}{z-\pi} - \mathrm{e}^{\pi} - \mathrm{e}^{\pi}\Big(\frac{1}{6} + \frac{1}{2}\Big)(z-\pi)
    >             -\mathrm{e}^{\pi} \Big(\frac{1}{6} + \frac{1}{6} \Big)(z-\pi)^{2} - \cdots
    >     \end{align*}
    >     which shows that the residue of $\frac{\mathrm{e}^{z}}{\sin z}$ at
    >     $z=\pi$ is $-\mathrm{e}^{\pi}$. Thus
    >     $$\oint_{|z-2| = 3} \frac{\mathrm{e}^{z}}{\sin z}\,\mathrm{d}z = 2\pi i \big( 1 - \mathrm{e}^{\pi} \big).$$

1.  Compute the residues of $\frac{1}{(z^{2}-4)(z-2)}$ at $z=2$, and $z=-2$;
    then use them to compute the integral
    $$\oint_{|z-2|=10}\frac{1}{(z^{2}-4)(z-2)}\,\mathrm{d}z.$$

    > **Solution:**
    >
    > Since the pole at $z=2$ is of order 2 (_Why?_), we have
    > \begin{align*}
    >   \mathrm{Res}\Big[\frac{1}{(z^{2}-4)(z-2)}, z=2 \Big]
    >     & = \frac{1}{1!} \lim_{z\to 2} \frac{\mathrm{d}}{\mathrm{d}z}
    >           \Big( (z-2)^{2} \frac{1}{(z^{2}-4)(z-2)}\Big) \\
    >     & = \lim_{z\to 2} \frac{\mathrm{d}}{\mathrm{d}z} \Big( \frac{1}{(z+2)} \Big) \\
    >     & = \lim_{z\to 2} -(z+2)^{-2} = -\frac{1}{16}.
    > \end{align*}
    >
    > The pole at $z=-2$ is of order one (_Why?_). Thus
    > \begin{align*}
    >   \mathrm{Res}\Big[\frac{1}{(z^{2}-4)(z-2)}, z=-2 \Big]
    >     & = \lim_{z\to -2} \Big( (z+2) \frac{1}{(z^{2}-4)(z-2)}\Big) \\
    >     & = \lim_{z\to -2} \frac{1}{(z-2)^{2}} = \frac{1}{16}.
    > \end{align*}
    >
    > The desired integral is then
    > $$\oint_{|z-2|=10}\frac{1}{(z^{2}-4)(z-2)}\,\mathrm{d}z = 2\pi i \big(
    > \frac{1}{16} - \frac{1}{16} \big) = 0.$$

1.  Find the residue at $z=0$ of

    i.  $z^{-3}\sin z$
    i.  $\frac{z^{2}+4z+5}{z^{2}+z}$

    > **Solution:**
    >
    > i.  First note that the pole at $z=0$ is of order two. This is because the
    >     function $\frac{\sin z}{z}$ is entire (_Why?_). Thus we have
    >     \begin{align*}
    >       \mathrm{Res}\big[z^{-3}\sin z, z=0 \big]
    >         & = \frac{1}{1!} \lim_{z\to 0} \frac{\mathrm{d}}{\mathrm{d}z}
    >               \big( z^{2} z^{-3} \sin z \big) \\
    >         & = \lim_{z\to 0} \frac{\mathrm{d}}{\mathrm{d}z}
    >               \big( \frac{\sin z}{z} \big) \\
    >         & = \lim_{z\to 0} \big( \frac{z\cos z - \sin z}{z^{2}} \big) \\
    >         & = \lim_{z\to 0} \big( \frac{-z\sin z + \cos z - \cos z}{2z} \big) \\
    >         & = \lim_{z\to 0} \big( \frac{-\sin z}{2} \big) \\
    >         & = -\frac{1}{2} \sin 0 \\
    >         & = 0,
    >     \end{align*}
    >     where _L'Hopital's_ rule has been used in the computation of the limit
    >     of the derivative of $\frac{\sin z}{z}$ at $z=0$.
    >
    > ii. Since the pole at $z=0$ is simple, we have
    >     \begin{align*}
    >       \mathrm{Res}\Big[ \frac{z^{2}+4z+5}{z^{2}+z}, z=0 \Big]
    >         & = \lim_{z\to 0} z \frac{z^{2}+4z+5}{z^{2}+z} \\
    >         & = \lim_{z\to 0} \frac{z^{2}+4z+5}{z+1} \\
    >         & = \frac{(0)^{2}+4(0)+5}{(0)+1} \\
    >         & = 5.
    >     \end{align*}

1.  Compute the _Laurent_ series of

    i.  $f(z) = \frac{1}{1+z^{2}}$ centered at $z=0$.
    i.  $g(z) = \frac{z}{(z-1)(z-2)}$ centered at $z=1$.

        > _Hint:_ Since $z= (z-1)+1$, then
        > $$\frac{z}{(z-1)(z-2)} = \frac{(z-1)+1}{(z-1)(z-2)} = \frac{1}{z-2}+\frac{1}{z-1}\cdot\frac{1}{z-2}.$$
        > Thus, it suffices to find the _Laurent_ series of $\frac{1}{z-2}$ around
        > $z=1$.

    i.  $h(z) = \frac{z-3}{(z-1)(z-2)}$ centered at $z=1$.

    > **Solution:**
    >
    > i.  Note that $1+z^{2} = 1 - q$, where $q = -z^{2}$. Hence
    >     \begin{align*}
    >       \frac{1}{1+z^{2}} & = \frac{1}{1-q} \\
    >         & = 1 + q + q^{2} + q^{3} + \cdots + q^{n} + \cdots \\
    >         & = 1 + (-z^{2}) + (-z^{2})^{2} + (-z^{2})^{3} + \cdots + (-z^{2})^{n} + \cdots \\
    >         & = 1 - z^{2} + z^{4} - z^{6} + \cdots + (-1)^{n}z^{2n} + \cdots
    >     \end{align*}
    >     is valid when $|q| < 1$. Since $q = z^{2}$, the series converges in
    >     the region $|z|^{2} < 1$ which is clearly equivalent to $|z|<1$. 
    >
    > ii. Note that
    >     \begin{align*}
    >       \frac{1}{z-2} & = -\frac{1}{1-(z-1)} \\
    >         & = -\big( 1 + (z-1) + (z-1)^{2} + (z-1)^{3} + \cdots + (z-1)^{n} + \cdots \big) \\
    >         & = -1 - (z-1) - (z-1)^{2} - (z-1)^{3} - \cdots - (z-1)^{n} - \cdots
    >     \end{align*}
    >     Hence, by the hint
    >     \begin{align*}
    >       \frac{z}{(z-1)(z-2)} = & \frac{1}{z-2}+\frac{1}{z-1}\cdot\frac{1}{z-2} \\
    >         = & -1 - (z-1) - (z-1)^{2} - (z-1)^{3} - \cdots - (z-1)^{n} - \cdots \\
    >           & + \frac{1}{z-1} \Big( -1 - (z-1) - (z-1)^{2} - (z-1)^{3} - \cdots - (z-1)^{n} - \cdots \Big) \\
    >         = & -\frac{1}{z-1} - 2 - 2(z-1) - 2(z-1)^{2} - 2(z-1)^{3} - \cdots - 2(z-1)^{n} - \cdots
    >     \end{align*}
    >     with convergence in the region $|z-1|<1$.
    >
    > iii. Note that
    >     \begin{align*}
    >       \frac{z-3}{(z-1)(z-2)} = & \frac{z}{(z-1)(z-2)} - \frac{3}{(z-1)}\frac{1}{(z-2)} \\
    >         = & -\frac{1}{z-1} - 2 - 2(z-1) - 2(z-1)^{2} - 2(z-1)^{3} - \cdots - 2(z-1)^{n} - \cdots \\
    >           & - \frac{3}{(z-1)} \Big( -1 - (z-1) - (z-1)^{2} - (z-1)^{3} - \cdots - (z-1)^{n} - \cdots \Big) \\
    >         = & \frac{2}{z-1} + 1 + (z-1) + (z-1)^{2} + (z-1)^{3} + \cdots + (z-1)^{n} + \cdots
    >     \end{align*}
    >     with convergence in the region $|z-1|<1$.

---

## Practice for midterm 2

_Status:_ Final.

> Additional exercises will not be added to this list.

1.  Find the lengths of the following paths:

    i.  $\gamma(t) = i + \mathrm{e}^{i\pi t}$, $0 \le t \le 1$.
    i.  $\gamma(t) = i\sin(t)$, $-\pi \le t \le \pi$.

    > **Solution:**
    >
    > i.  The length is
    >     $$\mathcal{L} = \int_{0}^{1} |\gamma'(t)|\,\mathrm{d}t =
    >       \int_{0}^{1} | i\pi\mathrm{e}^{i\pi t}|\,\mathrm{d}t =
    >       \int_{0}^{1} \pi\,\mathrm{d}t = \pi.$$
    > i.  The length is
    >     $$\mathcal{L} = \int_{-\pi}^{\pi} |\gamma'(t)|\,\mathrm{d}t =
    >       \int_{-\pi}^{\pi} |i\cos t|\,\mathrm{d}t =
    >       \int_{-\pi}^{\pi} |\cos t|\,\mathrm{d}t.$$
    >     Since within the interval $[-\pi,\pi]$ the _cosine_ function is non
    >     negative only between $-\pi/2$ and $\pi/2$, we have
    >     \begin{align*}
    >       \mathcal{L}
    >       & =  \int_{-\pi}^{-\pi/2} (-\cos t)\,\mathrm{d}t +
    >       \int_{-\pi/2}^{\pi/2} \cos t\,\mathrm{d}t +
    >       \int_{\pi/2}^{\pi} (-\cos t)\,\mathrm{d}t \\
    >       & = \big(-(-1)+0\big) + \big(1-(-1)\big) + \big(0+1\big) = 4.
    >     \end{align*}

1.  Integrate the following functions over the circle $|z| = 2$, positively
    oriented:

    i.  $f(z) = z + \bar{z}$
    i.  $f(z) = z^{2} - 2z + 3$

    > **Solution:**
    >
    > i.  Using the parametrization $z(t) = 2\mathrm{e}^{it}$, $0\le t\le 2\pi$,
    >     we have
    >     \begin{align*}
    >       \int_{|z|=2}z+\bar{z}\,\mathrm{d}z
    >       & = \int_{0}^{2\pi} (2\mathrm{e}^{it} +
    >       2\mathrm{e}^{-it})(2i\mathrm{e}^{it})\,\mathrm{d}t \\
    >       & =
    >       4i \int_{0}^{2\pi} \mathrm{e}^{2it} + 1\,\mathrm{d}t \\
    >       & = 4i \big(2\pi I(2) + 2\pi I(0)\big) = 8\pi i\big(I(2)+I(0)\big)\\
    >       & = 8\pi i (0 + 1) = 8\pi i,
    >     \end{align*}
    >     where $I(2)$ and $I(0)$ are the integral expressions from problem 5
    >     below.
    > i.  No parametrization is needed. The function has antiderivatives; in
    >     particular, if $\alpha$ is any point with magnitude equal to 2, and if
    >     we let $F(z) = z^{3}/3 - z^{2} + 3z$, we then have
    >     $$\int_{|z|=2} z^{2}-2z+3\,\mathrm{d}z = F(\alpha) - F(\alpha)=0.$$

1.  Evaluate $\int_{\gamma} \mathrm{e}^{3z}\,\mathrm{d}z$ for each of the
    following paths:

    i.  $\gamma$ is the line segment from 1 to $i$.
    i.  $\gamma$ is the arc of the parabola $y=x^{2}$ from $x=0$ to $x=1$.

    > **Solution:**
    >
    > i.  Let $F(z) = \mathrm{e}^{3z}/3$. We then have
    >     $$ \int_{\gamma} \mathrm{e}^{3z}\,\mathrm{d}z = F(i) - F(1) =
    >     \frac{\mathrm{e}^{3i}-\mathrm{e}^{3}}{3}.$$
    > i.  With $F(z)$ as in the previous problem, we have
    >     $$ \int_{\gamma} \mathrm{e}^{3z}\,\mathrm{d}z = F(1+i) - F(0) =
    >     \frac{\mathrm{e}^{3(1+i)}-1}{3}.$$

1.  Show that $\int_{\gamma} z^{n}\,\mathrm{d}z = 0$, for any closed piecewise
    smooth $\gamma$ and any integer $n \neq -1$. (If $n$ is negative, assume
    that $\gamma$ does not pass through the origin, since otherwise the integral
    is not defined.)

    > **Solution:**
    >
    > If $n$ is positive or zero, the integrand is _entire_ (_i.e.,_ anaylitic
    > in all of $\mathbb{C}$); hence the integral is 0. A similar argument shows
    > that if $n$ is negative and the origin is _on the outside_ of $\gamma$,
    > the integral is also zero because $z^{n}$ is analytic everywhere except at
    > $z=0$.
    >
    > If the origin is _inside_ $\gamma$ and the exponent $n$ is negative and
    > different than $-1$ (say $n=-m$, with $m \ge 2$), we have
    > $$\int_{\gamma}z^{n}\,\mathrm{d}z = \int_{\gamma}
    > \frac{f(z)}{(z-0)^{(m-1)+1}}\,\mathrm{d}z = 2\pi i
    > \frac{f^{(m-1)}(0)}{(m-1)!},$$
    > where $f(z)$ is the constant function equal to one everywhere in
    > $\mathbb{C}$. Since $f$ is constant, all of its derivatives vanish. In
    > particular $f^{(m-1)}(0) = 0$ (since $m\ge 2$), which shows that
    > $\int_{\gamma} z^{n}\,\mathrm{d}z = 0$.
    >
    > > _Note:_ In the argument above when $n=-1$ (_i.e.,_ $m = 1$), the
    > > integral equals $2\pi i f(0)/0! = 2\pi i (1) = 2\pi i$.


1.  Let
    $$I(k) := \frac{1}{2\pi} \int_{0}^{2\pi} \mathrm{e}^{ikt}\,\mathrm{d}t.$$

    i.  Show that $I(0) = 1$.
    i.  Show that $I(k) = 0$ if $k$ is a non-zero integer.

    > _Note:_ Are you integrating over a closed curve? or are you simply using
    > formulas from 31A?

    > **Solution:**
    >
    > Although the results are straightforward to obtain using _real calculus_
    > techniques, the idea here is to use complex integration. As I pointed out
    > during lecture, integrals over the interval $[0,2\pi]$ that involve
    > exponential functions (including _sines_, _cosines_, and the like) can be
    > expressed as complex integrals over the unit circle in $\mathbb{C}$. The
    > _trick_ is to use the typical parametrization $z(t)=\mathrm{e}^{it}$,
    > $0\le t \le 2\pi$, _in the opposite direction_. In order to do this we
    > introduce the term $z'(t) = i \mathrm{e}^{it}$, and then we identify the
    > remaining terms in this integrand as the composition $f\big(z(t)\big)$.
    >
    > If $k\neq 0$, we have
    > \begin{align*}
    >   I(k) & = \frac{1}{2\pi} \int_{0}^{2\pi} \mathrm{e}^{ikt}\,\mathrm{d}t
    >   = \frac{1}{2\pi} \int_{0}^{2\pi} \frac{\mathrm{e}^{ikt}
    >     \cdot i\mathrm{e}^{it}}{i\mathrm{e}^{it}}\,\mathrm{d}t \\
    >   & = \frac{1}{2\pi i} \int_{0}^{2\pi} \mathrm{e}^{i(k-1)t}
    >     \cdot i\mathrm{e}^{it}\,\mathrm{d}t \\
    >   & = \frac{1}{2\pi i} \int_{0}^{2\pi} \big(\mathrm{e}^{it}\big)^{k-1}
    >     \cdot i\mathrm{e}^{it}\,\mathrm{d}t \\
    >   & = \frac{1}{2\pi i} \int_{|z|=1} z^{k-1}\,\mathrm{d}z,
    > \end{align*}
    > which is equal to zero (see problem 4 above).
    >
    > Similarly when $k=0$,
    > $$I(0) = \frac{1}{2\pi i} \int_{|z|=1} \frac{1}{z}\,\mathrm{d}z =
    > \frac{1}{2\pi i}(2\pi i) = 1.$$


1.  Show that
    $$\int_{|z|=2019} \frac{1}{z^{3}+1}\,\mathrm{d}z = 0$$
    by arguing that this integral does not change if we replace $|z| = 2019$
    by the set $|z| = R$ for any $R>1$; then estimate the modulus of the
    integral and take the limit as $R$ goes to infinity.

    > **Solution:**
    >
    > Notice that the only places where the integrand fails to be analytic are
    > the points $z=i$ and $z=-i$, both of which are located _inside_ any circle
    > centered at the origin provided that the radius is bigger than one. This
    > means that if $R>1$,
    > $$I_{R} := \int_{|z|=R} \frac{1}{z^{3}+1}\,\mathrm{d}z
    > = \int_{|z|=2019} \frac{1}{z^{3}+1}\,\mathrm{d}z.$$
    > On the other hand,
    > $$|I_{R}| = \Big| \int_{|z|=R} \frac{1}{z^{3}+1}\,\mathrm{d}z \Big|
    >   \le 2\pi R\cdot \max_{|z|=R} \frac{1}{|z^{3} + 1|}.$$
    > To conclude, notice that by the triangle inequality
    > $$\big| |z^{3}| - |-1| \big| \le |z^{3} - (-1)| = |z^{3} + 1 |,$$
    > and in particular on the set $\{z\,:\, |z| = R > 1 \}$ this becomes
    > $$R^{3} - 1 \le |z^{3} + 1|,$$
    > which in turn leads to
    > $$\frac{1}{|z^{3}+1|} \le \frac{1}{R^{3}-1}$$
    > whenever $|z| = R >1$.
    >
    > We then have for $R>1$,
    > $$|I_{R}| \le \frac{2\pi R}{R^{3}-1}.$$
    > Taking the limit as $R\to\infty$ on both sides of the inequality above
    > leads to
    > $$\lim_{R\to\infty}\Big|\int_{|z|=2019}\frac{1}{z^{3}+1}\,\mathrm{d}z\Big|
    >   = \lim_{R\to\infty}|I_{R}| \le \lim_{R\to\infty} \frac{2\pi R}{R^{3}-1}
    >   = 0.$$
    > Since the left hand side above does not depend on $R$, we conclude that
    > $$\Big| \int_{|z|=2019} \frac{1}{z^{3}+1}\,\mathrm{d}z \Big| = 0.$$
    > Therefore
    > $$\int_{|z|=2019} \frac{1}{z^{3}+1}\,\mathrm{d}z = 0.$$

1.  Compute
    $$I(r) := \int_{\mathcal{C}_{r,-2i}} \frac{1}{z^{2}+1}\,\mathrm{d}z$$
    for $r \neq 1, 3$; where $\mathcal{C}_{r,a} =\{ z\,:\, |z-a| = r\}$.

    > **Solution:**
    >
    > The points $z=-i$ and $z=i$ (located at distances $r=1$ and $r=3$ from
    > $2i$, respectively) are the only places where the integrand is not
    > analytic. We then have:
    >
    > *   For $r<1$, $I(r) = 0$ because $1/(z^{2}+1)$ is analytic _inside_ the
    >     contour of integration.
    > *   For values of $r$ between 1 and 3, _Cauchy's integral formula_ leads
    >     to
    >     $$\int_{\mathcal{C}_{r,-2i}} \frac{1}{z^{2}+1}\,\mathrm{d}z
    >       = \int_{\mathcal{C}_{r,-2i}} \frac{f_{1}(z)}{z-(-i)}\,\mathrm{d}z
    >       = (2\pi i) \cdot f_{1}(-i),$$
    >     where $f_{1}(z) = 1/(z-i)$ is analytic in $\mathcal{C}_{r,-2i}$. Thus,
    >     we have
    >     $$\int_{\mathcal{C}_{r,-2i}} \frac{1}{z^{2}+1}\,\mathrm{d}z
    >       = (2\pi i) \cdot \frac{1}{(-i)-i} = \frac{2\pi i}{-2i} = -\pi.$$
    > *   For $r>3$, a contour deformation argument leads to
    >     \begin{align*}
    >       \int_{\mathcal{C}_{r,-2i}} \frac{1}{z^{2}+1}\,\mathrm{d}z
    >       & = \int_{\mathcal{C}_{1/2,-i}} \frac{1}{z^{2}+1}\,\mathrm{d}z +
    >         \int_{\mathcal{C}_{1/2,i}} \frac{1}{z^{2}+1}\,\mathrm{d}z \\
    >       & = 2\pi i \cdot f_{1}(-i) + 2\pi i \cdot f_{2}(i),
    >     \end{align*}
    >     where $f_{1}(z)$ is as above, and $f_{2}(z) = 1/(z+i)$. Thus
    >     $$\int_{\mathcal{C}_{r,-2i}} \frac{1}{z^{2}+1}\,\mathrm{d}z
    >       = (2\pi i) \cdot \frac{1}{(-i)-i} + (2\pi i) \cdot \frac{1}{(i)+i}
    >       = \frac{2\pi i}{-2i} + \frac{2\pi i}{2i}= -\pi + \pi = 0.$$

1.  Compute the following integrals, where $\square$ is the boundary of the
    square with vertices at $\pm 4 \pm 4i$, positively oriented:

    i.  $$\int_{\square}\frac{\mathrm{e}^{z^{2}}}{z^{3}}\,\mathrm{d}z$$
    i.  $$\int_{\square}\frac{\sin(2z)}{(z-\pi)^{2}}\,\mathrm{d}z$$

    > **Solution:**
    >
    > i.  Since the point $z=0$ is located _inside_ contour in the integration
    >     contour, we have
    >     $$\int_{\square}\frac{\mathrm{e}^{z^{2}}}{z^{3}}\,\mathrm{d}z
    >       = \int_{\square}\frac{\mathrm{e}^{z^{2}}}{(z-0)^{2+1}}\,\mathrm{d}z
    >       = \frac{2\pi i}{2!}\cdot f''(0),$$
    >     where $f(z) = \mathrm{e}^{z^{2}}$. The first and second derivatives of
    >     $f(z)$ are $f'(z) = 2z\mathrm{e}^{z^{2}}$, and
    >     $f''(z)=2(2z^{2}+1)\mathrm{e}^{z^{2}}$. Since $f''(0) = 2$,
    >     $$\int_{\square}\frac{\mathrm{e}^{z^{2}}}{z^{3}}\,\mathrm{d}z = 
    >     \frac{2\pi i}{2!}\cdot (2) = 2\pi i.$$
    > i.  Since the point $z=\pi$ is also located _inside_ contour in the
    >     integration, we have
    >     $$\int_{\square}\frac{\sin(2z)}{(z-\pi)^{2}}\,\mathrm{d}z
    >       \int_{\square}\frac{\sin(2z)}{(z-\pi)^{1+1}}\,\mathrm{d}z
    >       = \frac{2\pi i}{1!}\cdot f'(\pi),$$
    >     where $f(z) = \sin(2z)$. This leads to
    >     $$\int_{\square}\frac{\sin(2z)}{(z-\pi)^{2}}\,\mathrm{d}z
    >       = \frac{2\pi i}{1!}\cdot 2\cos(2\pi) = 4\pi i.$$

1.  Integrate the following functions over the circle $|z| = 3$, positively
    oriented:

    i. $\frac{1}{z-1/2}$
    i. $\big(\frac{\cos z}{z}\big)^{2}$
    i. $\frac{1}{z^{2}-4}$

    > **Partial solution:**
    >
    > See questions 7 and 8 for details needed to complete the arguments below.
    >
    > i.  Since $|1/2| < 3$ (_i.e.,_ $1/2$ is inside the circle with radius 3),
    >     $$\int_{|z|=3} \frac{1}{z-1/2}\,\mathrm{d}z
    >       = 2\pi i \cdot f(1/2),$$
    >     where $f(z) = 1$.
    > i.  Since $|0|<3$,
    >     $$\int_{|z|=3} \Big(\frac{\cos z}{z}\Big)^{2}\,\mathrm{d}z
    >       = \int_{|z|=3} \frac{\cos^{2} z}{(z-0)^{1+1}}\,\mathrm{d}z
    >       = \frac{2\pi i}{1!}\cdot g'(0),$$
    >     where $g(z) = \cos^{2} z$.
    > i.  Since $|\pm 2|<3$,
    >     $$\int_{|z|=3} \frac{1}{z^{2}-4} \mathrm{d}z
    >       = 2\pi i \cdot h_{1}(-2) + 2\pi i \cdot h_{2}(2),$$
    >     where $h_{1}(z) = 1/(z-2)$, and $h_{2}(z) = 1/(z+2)$.

1.  Compute

    $$\int_{-\infty}^{\infty}\frac{1}{x^{4}+1}\,\mathrm{d}x.$$

    > _Hint:_
    >
    > Integrate the function $f(z) = \frac{1}{z^{4}+1}$ over the set
    > $\sigma_{R} = \Gamma_{[-R,R]} + \Gamma_{S^{+}_{R}}$; where
    >
    > *   $\Gamma_{[-R,R]} = \{x + 0i\,:\, -R \le x \le R \}$, and
    > *   $\Gamma_{S^{+}_{R}} = \{ R\mathrm{e}^{it}\,:\, 0 \le t \le \pi \}$.
    >
    > That is, $\sigma_{R}$ is the counterclockwise semicircle formed by
    > the segment $[-R,R]$ on the real axis, followed by the circular arc of
    > radius $R$ in the upper half plane from $R$ to $-R$. Conclude your
    > computations by taking the limit as $R$ goes to infinity.
    >
    > **Update:** The original description of the set $\Gamma_{S^{+}_{R}}$ was
    > missing the _radius_ $R$.

    > **Solution:**
    >
    > Let
    > \begin{align*}
    >   z_{1} & = (1+i)/\sqrt{2}, &
    >   z_{2} & = (-1+i)/\sqrt{2}, \\
    >   z_{3} & = (-1-i)/\sqrt{2}, &
    >   z_{4} & = (1-i)/\sqrt{2},
    > \end{align*}
    > it is easy to see that for $j = 1,\dots,4$, the number $z_{j}$ is a root
    > of the polynomial $z^{4} + 1 = 0$. Indeed,
    > $$\Big(\frac{\pm 1 \pm i}{\sqrt{2}}\Big)^{4}
    >   = \Big(\frac{(\pm 1 \pm i)^{2}}{(\sqrt{2})^{2}}\Big)^{2}
    >   = \Big(\frac{1 \pm 2i -1}{2}\Big)^{2} = (\pm i)^{2} = -1.$$
    > In other
    > words, $z^{4} + 1 = (z-z_{1})(z-z_{2})(z-z_{3})(z-z_{4})$.
    >
    > Next, notice that if $R>1$, only $z_{1}$ and $z_{2}$ are located _inside_
    > $\sigma_{R}$. Therefore
    > $$\int_{\sigma_{R}} \frac{1}{z^{4}+1}\,\mathrm{d}z
    > = 2\pi i \big( f_{1}(z_{1}) + f_{2}(z_{2}) \big),$$
    > where
    > \begin{align*}
    >   f_{1}(z) & = \frac{1}{(z-z_{2})(z-z_{3})(z-z_{4})}, \\
    >   f_{2}(z) & = \frac{1}{(z-z_{1})(z-z_{3})(z-z_{4})}.
    > \end{align*}
    > Plugging in $z_{1}$ in the first expression above leads to
    > \begin{align*}
    >   f_{1}(z_{1}) & = \frac{1}{(z_{1}-z_{2})(z_{1}-z_{3})(z_{1}-z_{4})} \\
    >   & = \frac{1}{\big(\sqrt{2}\big)\big(\sqrt{2}(1+i)\big)\big(i\sqrt{2}\big)} \\
    >   & = \frac{1}{i2\sqrt{2}(1+i)}.
    > \end{align*}
    > Similarly, for the second expression we have
    > \begin{align*}
    >   f_{2}(z_{2}) & = \frac{1}{(z_{2}-z_{1})(z_{2}-z_{3})(z_{2}-z_{4})} \\
    >   & = \frac{1}{\big(-\sqrt{2}\big)\big(i\sqrt{2}\big)\big(\sqrt{2}(i-1)\big)} \\
    >   & = \frac{1}{i2\sqrt{2}(1-i)},
    > \end{align*}
    > These two observations lead to
    > \begin{align*}
    >   2\pi i\big(f_{1}(z_{1}) + f_{2}(z_{2})\big)
    >   & = \frac{\pi}{\sqrt{2}}\Big(\frac{1}{1+i} + \frac{1}{1-i} \Big) \\
    >   & = \frac{\pi}{\sqrt{2}}\Big(\frac{1-i+1+i}{1-i^{2}}\Big) \\
    >   & = \frac{\pi}{\sqrt{2}}.
    > \end{align*}
    > Summarizing,
    > $$\int_{\sigma_{R}} \frac{1}{z^{4}+1}\,\mathrm{d}z = \frac{\pi}{\sqrt{2}}$$
    > holds whenever $R>1$.
    >
    > On the other hand
    > $$\int_{\Gamma_{[-R,R]}} \frac{1}{z^{4}+1}\,\mathrm{d}z =
    >   \int_{-R}^{R} \frac{1}{x^{4}+1}\,\mathrm{d}x,$$
    > and a similar analysis to the one in problem 6 above leads to the
    > following bound
    > $$\Big| \int_{\Gamma_{S^{+}_{R}}} \frac{1}{z^{4}+1}\,\mathrm{d}z \Big|
    > \le \frac{\pi R}{R^{4}-1},$$
    > which shows that as $R\to\infty$, the contribution from the integral over
    > $\Gamma_{S^{+}_{R}}$ vanishes. Since $\sigma_{R} = \Gamma_{[-R,R]} +
    > \Gamma_{S^{+}_{R}}$, we have[^careful]
    > \begin{align*}
    >   \int_{-\infty}^{\infty}\frac{1}{x^{4}+1}\,\mathrm{d}x
    >   & = \lim_{R\to\infty} \int_{\Gamma_{[-R,R]}} \frac{1}{z^{4}+1}\,\mathrm{d}z \\
    >   & = \lim_{R\to\infty} \int_{\sigma_{R}} \frac{1}{z^{4}+1}\,\mathrm{d}z
    >     - \lim_{R\to\infty} \int_{\Gamma_{S^{+}_{R}}} \frac{1}{z^{4}+1}\,\mathrm{d}z \\
    >   & = \lim_{R\to\infty} \frac{\pi}{\sqrt{2}} - 0 = \frac{\pi}{\sqrt{2}}.
    > \end{align*}

[^careful]: The first equality in the chain is actually not correctly justified.
  Can you explain why?

---

## Practice for midterm 1

_Status:_ Final.  

> Additional exercises will not be added to this list.

1.  Find all the solutions of the equation $z^{5} - 1 = 0$; then explain why it
    is true that

    $$z^{5} - 1 = (z-1)(z^{2}+2z\cos(\pi/5)+1)(z^{2}-2z\cos(2\pi/5)+1).$$

    > **Solution:**
    >
    > The roots of the equation are $z_{1}=1$, $z_{2} = \mathrm{e}^{2\pi/5}$,
    > $z_{3} = \mathrm{e}^{4\pi/5}$, $z_{4} = \bar{z_{3}}$, and $z_{5} =
    > \bar{z_{2}}$. Why?
    >
    > It follows that
    >
    > \begin{align*}
    >   z^{5}-1 & = (z-z_{1})(z-z_{2})(z-z_{3})(z-\bar{z_{3}})(z-\bar{z_{2}}) \\
    >     & = (z-1)(z-z_{2})(z-\bar{z_{2}})(z-z_{3})(z-\bar{z_{3}}) \\
    >     & = (z-1)(z^{2}-zz_{2} -z\bar{z_{2}} + |z_{2}|^{2})
    >         (z^{2}-zz_{3} -z\bar{z_{3}} + |z_{3}|^{2}) \\
    >     & = (z-1)(z^{2}-z(z_{2} +\bar{z_{2}}) + |z_{2}|^{2})
    >         (z^{2}-z(z_{3} + \bar{z_{3}}) + |z_{3}|^{2}).
    > \end{align*}
    >
    > Next, notice that
    > \begin{align*}
    >   z_{2} +\bar{z_{2}} & = 2\mathbf{Re}(z_{2}) = 2\cos(2\pi/5), \\
    >   z_{3} +\bar{z_{3}} & = 2\mathbf{Re}(z_{3}) = 2\cos(4\pi/5),
    > \end{align*}
    >
    > which leads to
    >
    > $$ z^{5}-1 = (z-1)(z^{2}-2z\cos(2\pi/5) + 1)(z^{2}-2z\cos(4\pi/5) + 1).$$
    >
    > To conclude, note that $4\pi/5 = \pi - \pi/5$, and that
    >
    > $$\cos(\pi-\pi/5) = \cos(\pi)\cos(\pi/5) + \sin(\pi)\sin(\pi/5) = -\cos(\pi/5).$$


1.  Prove that for all $z,z_{1},z_{2}\in\mathbb{C}$,

    i.  $\sin(z+2\pi) = \sin(z)$
    i.  $\tan(z+\pi) = \tan(z)$
    i.  $\sin(z_{1}\pm z_{2}) = \sin(z_{1})\cos(z_{2}) \pm \sin(z_{2})\cos(z_{1})$

    > **Solution:**
    >
    > i.  $$\sin(z+2\pi) = \frac{\mathrm{e}^{i(z+2\pi)}-\mathrm{e}^{-i(z+2\pi)}}{2i}
    >       = \frac{\mathrm{e}^{2\pi i}\mathrm{e}^{iz}-\mathrm{e}^{-2\pi i}\mathrm{e}^{-iz}}{2i}
    >       = \frac{\mathrm{e}^{iz}-\mathrm{e}^{-iz}}{2i} = \sin(z)$$
    > i.  \begin{align*}
    >       \tan(z+\pi) & = \frac{\sin(z+\pi)}{\cos(z+\pi)}
    >       = \frac{\mathrm{e}^{i(z+\pi)}-\mathrm{e}^{-i(z+\pi)}}{i(\mathrm{e}^{i(z+\pi)}+\mathrm{e}^{-i(z+\pi)})}
    >       \cdot \frac{\mathrm{e}^{i\pi}}{\mathrm{e}^{i\pi}} \\
    >       & = \frac{\mathrm{e}^{2\pi i}\mathrm{e}^{iz}-\mathrm{e}^{-iz}}{i(\mathrm{e}^{2\pi i}\mathrm{e}^{iz}+\mathrm{e}^{-iz})}
    >         = \frac{\mathrm{e}^{iz}-\mathrm{e}^{-iz}}{i(\mathrm{e}^{iz}+\mathrm{e}^{-iz})}
    >         = \tan(z)
    >     \end{align*}
    > i.  \begin{align*}
    >       \sin(z_{1})\cos(z_{2}) + \sin(z_{2})\cos(z_{1})
    >       = & \frac{\mathrm{e}^{iz_{1}}-\mathrm{e}^{-iz_{1}}}{2i} \cdot
    >           \frac{\mathrm{e}^{iz_{2}}+\mathrm{e}^{-iz_{2}}}{2}
    >         + \frac{\mathrm{e}^{iz_{2}}-\mathrm{e}^{-iz_{2}}}{2i} \cdot
    >           \frac{\mathrm{e}^{iz_{1}}+\mathrm{e}^{-iz_{1}}}{2i} \\
    >       = & \frac{\mathrm{e}^{i(z_{1}+z_{2})}+\mathrm{e}^{i(z_{1}-z_{2})}
    >               -\mathrm{e}^{-i(z_{1}-z_{2})}-\mathrm{e}^{-i(z_{1}+z_{2})}}{4i} \\
    >         & + \frac{\mathrm{e}^{i(z_{2}+z_{1})}+\mathrm{e}^{i(z_{2}-z_{1})}
    >               -\mathrm{e}^{-i(z_{2}-z_{1})}-\mathrm{e}^{-i(z_{2}+z_{1})}}{4i} \\
    >       = & \frac{\mathrm{e}^{i(z_{1}+z_{2})}+\mathrm{e}^{i(z_{1}-z_{2})}
    >               -\mathrm{e}^{-i(z_{1}-z_{2})}-\mathrm{e}^{-i(z_{1}+z_{2})}}{4i} \\
    >         & + \frac{\mathrm{e}^{i(z_{1}+z_{2})}+\mathrm{e}^{-i(z_{1}-z_{2})}
    >               -\mathrm{e}^{i(z_{1}-z_{2})}-\mathrm{e}^{-i(z_{1}+z_{2})}}{4i} \\
    >       = & \frac{2\mathrm{e}^{i(z_{1}+z_{2})}-2\mathrm{e}^{-i(z_{1}+z_{2})}}{4i}
    >       = \frac{\mathrm{e}^{i(z_{1}+z_{2})}-\mathrm{e}^{-i(z_{1}+z_{2})}}{2i} \\
    >       = & \sin(z_{1}+z_{2})
    >     \end{align*}
    >
    >     A similar result holds for $\sin(z_{1}-z_{2})$.

1.  Let $f$ be a _complex-valued_ function such that for all points $z$ in the
    disk $D_{\rho}=\{z\,:\,|z-z_{0}| < \rho\}$

    $$f(z) = f(z_{0}) + \mathcal{M}(z-z_{0}) + \varepsilon(z)(z-z_{0}),$$

    for some fixed complex number $\mathcal{M}$, and some complex valued
    function $\varepsilon(z)$ satisfying

    $$\lim_{z\to z_{0}} \varepsilon(z) = 0.$$

    Show that $f$ is diferentiable at $z_{0}$. Moreover, $\mathcal{M}$ equals
    $f'(z_{0})$.

    > **Solution:**
    >
    > We have
    >
    > \begin{align*}
    >   \frac{f(z)-f(z_{0})}{z-z_{0}}
    >     & = \frac{f(z_{0}) + \mathcal{M}(z-z_{0}) + \varepsilon(z)(z-z_{0}) - f(z_{0})}{z-z_{0}} \\
    >     & = \frac{\mathcal{M}(z-z_{0}) + \varepsilon(z)(z-z_{0})}{z-z_{0}}
    >       = \mathcal{M} + \varepsilon(z).
    > \end{align*}
    >
    > Since both limits on the right hand side of the above equation exist, we
    > then have
    >
    > $$ f'(z_{0}) = \lim_{z\to z_{0}} \big(\mathcal{M} + \varepsilon(z)\big) =
    >   \mathcal{M}.$$

1.  Show that the function

    $$u(x,y) = \frac{x^{2}-y^{2}}{(x^{2}+y^{2})^{2}}$$

    is harmonic everywhere in $\mathcal{D}=\{(x,y)\,:\, x\neq 0,\, y\neq 0\}$.

    > _Hint:_ Study the function $f(z) = \frac{1}{z^{2}}$.
    >
    > **Solution:**
    >
    > Note that
    >
    > $$\frac{1}{z^{2}} = \frac{\bar{z}^{2}}{z^{2}\bar{z}^{2}} =
    >   \frac{(x-iy)^{2}}{(|z|^{2})^{2}} = \frac{x^{2}-y^{2}}{(x^{2}+y^{2})^{2}}
    >     -i\frac{2xy}{(x^{2}+y^{2})^{2}}.$$
    >
    > That is, $u(x,y) = \mathbf{Re}(1/z^{2})$. Hence $u(x,y)$ is harmonic
    > whenever $1/z^{2}$ is analytic. This happens precisely on the set
    > $\mathcal{D}$.

1.  Describe the images of the following sets under the exponential function
    $\mathrm{e}^{x+iy} = \mathrm{e}^{x}(\cos x + i\sin y)$:

    i.  the line segment defined by $z=iy$, $0\le y \le 2\pi$,
    i.  the line segment defined by $z=1+iy$, $0\le y \le 2\pi$,
    i.  the rectangle $\{z = x + iy \in \mathbb{C} \,:\, 0\le x \le 1,\, 0\le y
        \le 2\pi\}$.

    > **Solution:**
    >
    > i.  The image is the circle centered at 0, with radius equal to 1.
    > i.  The image is the circle centered at 0, with radius equal to
    >     $\mathrm{e}\approx 2.7172$.
    > i.  The image is the _washer_ centered at 0, with inner radius equal to 1,
    >     and outer radius equal to $\mathrm{e} \approx 2.7172$.

1.  Find all the values associated to the expressions below:

    i.  $\log(2i)$
    i.  $(-1)^{i}$
    i.  $\mathrm{Log}\,(-1+i)$

    > **Solution:**
    >
    > i.  $\log(2i) = \mathrm{Log}\,|2| + i(\pi/2 + 2\pi k)$, $k = 0, \pm 1, \pm 2, \dots$
    > i.  $(-1)^{i} = \mathrm{e}^{i\log(-1)} = \mathrm{e}^{i\big(\log|-1| + i(\pi + 2\pi k)\big)}
    >     = \mathrm{e}^{-(\pi + 2\pi k)}$, $k=0,\pm1,\pm2,\dots$
    > i.  $\mathrm{Log}\,(-1+i) = \mathrm{Log}\,\sqrt{2} + i3\pi/4$.

1.  Prove that if $f$ is differentiable in $\Omega \subseteq \mathbb{C}$ and
    always real-valued (that is, for all $z \in \Omega$, $f(z) \in \mathbb{R}$),
    then $f$ is constant in $\Omega$.

    > **Solution:**
    >
    > Since $f$ is real valued, $f = \bar{f}$. Or in other words, the
    > _Cauchy-Riemann_ equations hold for $f = u + iv$, and for $\bar{f} = u -
    > iv$. This leads to the four equations below
    >
    > \begin{align*}
    >   u_{x} & = v_{y} & u_{x} & = (-v)_{y}, \\
    >   u_{y} & = -v_{x} & u_{y} & = -(-v)_{x}.
    > \end{align*}
    >
    > From the firs row above we get $v_{y} = -v_{y}$, hence $v_{y} = 0$; and
    > from the second row we get $-v_{x} = v_{x}$, or $v_{x} = 0$. This in turn
    > leads to all partial derivatives ($u_{x},u_{y},v_{x},v_{y}$) being equal to 0.
    > In particular, since $f'(z) = u_{x} + i v_{x}$ we conclude that $f'(z)
    > =0$, and that $f$ must be a constant function.

---


[Return to main course website][MAIN]


[book2]: http://math.sfsu.edu/beck/complex.html
[MAIN]: ..

